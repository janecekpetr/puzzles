package cz.slanec.utils;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.LinkedList;
import java.util.List;

public class Digits {
	
	private Digits() {
		// nothing to do
	}
	
	/** Returns a number composed out of the digits in the order they are given. */
	public static long composeNumber(Iterable<Byte> digits) {
		long result = 0;
		for (byte digit : digits) {
			result = result*10 + digit;
		}
		return result;
	}

	/** Returns a list of digits in the order they appear in the original number. */
	public static List<Byte> decomposeNumber(long num) {
		checkArgument(num >= 0, "Non-negative number needed for decomposing. " + num + " received.");
		LinkedList<Byte> digits = new LinkedList<>();
		while (num > 0) {
			digits.addFirst((byte)(num % 10));
			num /= 10;
		}
		return digits;
	}

	/** Returns the digit at the specified index of a String. Returns bogus if the specified character is no digit. */
	public static byte digit(CharSequence str, int index) {
		final int length = str.length();
		checkArgument(index < length, "Index (%s) is greater that the String length (%s).", index, length);
		return (byte)(str.charAt(index) - '0');
	}

	/** Returns the digit at the specified index. */
	public static byte digit(long number, int index) {
		final int length = Commons.length(number);
		checkArgument(index < length, "Index (%s) is greater that the String length (%s).", index, length);
		return (byte)((number % Commons.powLong(10, length-index)) / Commons.powLong(10, length-index-1));
	}

	/** Returns the sum of digits of a number. Returns bogus if the String contains anything else than digits. */
	public static int sumDigits(CharSequence str) {
		int sum = 0;
		for (int i = 0; i < str.length(); i++) {
			sum += str.charAt(i) - '0';
		}
		return sum;
	}

	/** Returns the sum of digits of a number. */
	public static int sumDigits(int num) {
		int sum = 0;
		while (num > 0) {
			sum += num % 10;
			num /= 10;
		}
		return sum;
	}
}