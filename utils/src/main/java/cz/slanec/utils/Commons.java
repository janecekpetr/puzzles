package cz.slanec.utils;

import static com.google.common.base.Preconditions.checkArgument;

import java.nio.charset.StandardCharsets;

import cz.slanec.factorization.Factor;
import cz.slanec.factorization.Factorization;

public class Commons {
	
	private Commons() {
		// nothing to do
	}
	
	/** Returns a number obtained by concatenating the two provided numbers as if they were Strings. */
	public static long concatenate(int i, int j) {
		return (i*powLong(10, length(j)) + j);
	}
	
	/** Returns a number obtained by concatenating all the parameters as if they were Strings. */
	public static long concatenate(int... nums) {
		checkArgument(nums.length > 1, "Expected at least two numbers to concatenate!");
		
		long result = 0;
		for (int num : nums) {
			result = result*powLong(10, length(num)) + num;
		}
		return result;
	}
	
	/** Returns <code>true</code> iff the the dividend is divisable by the divisor. */
	public static boolean isDivisable(long dividend, long divisor) {
		return (dividend % divisor == 0);
	}
	
	/** Returns <code>true</code> iff the provided number is a perfect int. */
	public static boolean isInteger(double d) {
		return (d == (int)d);
	}
	
	public static boolean isEven(long num) {
		return ((num & 1) == 0);
	}
	
	public static boolean isOdd(long num) {
		return ((num & 1) == 1);
	}
	
	/** Returns <code>true</code> iff the String is a palindrome. */
	public static boolean isPalindrome(CharSequence str) {
		for (int i = 0; i < str.length()/2; i++) {
			if (str.charAt(i) != str.charAt(str.length()-1 - i)) {
				return false;
			}
		}
		return true;
	}

	/** Returns <code>true</code> iff the nnumber is a palindrome. */
	public static boolean isPalindrome(long num) {
		return isPalindrome(String.valueOf(num));
	}
	
	/**
	 * A utility method for determining the number of characters a number would be
	 * represented with. The call to this method is functionally equivalent to
	 * <pre> String.valueOf(number).length()</pre>
	 * <p>
	 * Examples:<pre>length(   1) == 1
	 *length(  10) == 2
	 *length( 100) == 3
	 *length(-100) == 4</pre>
	 * @return The number of decimal digits the number is consisting of (including the
	 *         minus sign if present).
	 */
	public static int length(long number) {
		// the minus sign or the special zero case have one more digit
		int length = (number <= 0) ? 1 : 0;
		while (number != 0) {
			length++;
			number /= 10;
		}
		return length;
	}

	/** Integer pow() function. Does not check for overflow. */
	public static int powInt(int base, int exp) {
		checkArgument(exp >= 0, "Exponent (%s) must be positive.", exp);
		
		if (exp == 0) {
			return 1;
		}
		
		// for low exponents, obvious multiplication is faster by a lot
		if (exp <= 10) {
			int result = base;
			while (exp --> 1) {
				result *= base;
			}
			return result;
		}
		
		// exponentiation by squaring
		int result = 1;
		while (exp > 0) {
			if ((exp & 1) == 1) {
				result *= base;
			}
			base *= base;
			exp >>= 1;
		}
		return result;
	}

	/** Long pow() function. Does not check for overflow. */
	public static long powLong(long base, int exp) {
		checkArgument(exp >= 0, "Exponent (%s) must be positive.", exp);
		
		if (exp == 0) {
			return 1;
		}
		
		// for low exponents, obvious multiplication is faster by a lot
		if (exp <= 10) {
			long result = base;
			while (exp --> 1) {
				result *= base;
			}
			return result;
		}
		
		// exponentiation by squaring
		long result = 1;
		while (exp > 0) {
			if ((exp & 1) == 1) {
				result *= base;
			}
			base *= base;
			exp >>= 1;
		}
		return result;
	}
	
	/** Returns the product of the provided numbers. */
	public static long product(Iterable<? extends Number> numbers) {
		long product = 1;
		for (Number number : numbers) {
			product *= number.intValue();
		}
		return product;
	}

	/** Returns the product of the provided numbers. */
	public static long product(int[] numbers) {
		long product = 1;
		for (int number : numbers) {
			product *= number;
		}
		return product;
	}

	/** Returns the sum of the provided numbers. */
	public static int sum(Iterable<? extends Number> numbers) {
		int sum = 0;
		for (Number number : numbers) {
			sum += number.intValue();
		}
		return sum;
	}
	
	/** Returns the sum of the provided numbers. */
	public static int sum(int[] numbers) {
		int sum = 0;
		for (int number : numbers) {
			sum += number;
		}
		return sum;
	}
	
	/** Returns the sum of the string character by character. Digits are not taken into account and 'A' has a value of 1. */
	public static int sumWord(CharSequence str) {
		int sum = 0;
		for (int i = 0; i < str.length(); i++) {
			sum += str.charAt(i) -'A' +1;
		}
		return sum;
	}
	
	/**
	 * Returns a number of relative primes to a number n. Or, rather, counts all numbers less than n that don't divide n.
	 * @see <a href=http://en.wikipedia.org/wiki/Euler%27s_totient_function>wiki</a>
	 */
	public static long totient(long n) {
		for (Factor factor : Factorization.factorize(n)) {
			n /= factor.factor;
			n *= (factor.factor-1);
		}
		return n;
	}
	
	public static byte[] xor(byte[] orig, byte key) {
		return xor(orig, new byte[] {key});
	}

	public static byte[] xor(byte[] orig, byte[] key) {
		byte[] result = new byte[orig.length];
		for (int i = 0; i < orig.length; i++) {
			result[i] = (byte)(orig[i] ^ key[i % key.length]);
		}
		return result;
	}

	public static int hammingDistance(byte a, byte b) {
		return Integer.bitCount(a ^ b);
	}

	public static int hammingDistance(byte[] a, byte[] b) {
		checkArgument(a.length == b.length, "Arrays of different lengths found!");
		
		int diff = 0;
		for (int i = 0; i < a.length; i++) {
			diff += hammingDistance(a[i], b[i]);
		}
		return diff;
	}

	public static int hammingDistance(String a, String b) {
		checkArgument(a.length() == b.length(), "Strings of different lengths found!");
		return hammingDistance(a.getBytes(StandardCharsets.UTF_8), b.getBytes(StandardCharsets.UTF_8));
	}
}