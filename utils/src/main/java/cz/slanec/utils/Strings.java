package cz.slanec.utils;

import java.nio.charset.StandardCharsets;
import java.util.HexFormat;

public class Strings {
	public static final char PRINTABLE_CHAR_LO = 32;	// a space: ' '
	public static final char PRINTABLE_CHAR_HI = 126;	// a tilde: '~'

	private Strings() {
		// nothing to do
	}

	public static String of(byte[] raw) {
		return new String(raw, StandardCharsets.UTF_8);
	}

	/** Returns <code>true</code> iff the given character is printable. */
	public static boolean isPrintable(char c) {
		final boolean isPrintable = (c >= PRINTABLE_CHAR_LO) && (c <= PRINTABLE_CHAR_HI);
		final boolean isControlChar = (c == '\n') || (c == '\r') || (c == '\t');
		return isPrintable || isControlChar;
	}

	/** Returns <code>true</code> iff the given CharSequence is printable. */
	public static boolean isPrintable(CharSequence str) {
		for (int i = 0; i < str.length(); i++) {
			if (!isPrintable(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static String toHexString(byte[] array) {
		return HexFormat.of().formatHex(array);
	}

	public static byte[] parseHexString(CharSequence str) {
		return HexFormat.of().parseHex(str);
	}
}