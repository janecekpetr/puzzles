package cz.slanec.gui;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

public class KeyStrokes {
	// shortcut calls
	public static KeyStroke ctrl(char key) {
		return new Builder().ctrl().key(key);
	}
	
	public static KeyStroke alt(char key) {
		return new Builder().alt().key(key);
	}
	
	public static KeyStroke shift(char key) {
		return new Builder().shift().key(key);
	}
	
	public static KeyStroke key(char key) {
		return new Builder().key(key);
	}
	
    public static KeyStroke keyReleased(char key) {
        return new Builder().keyReleased(key);
    }
	
	// Builder calls
	public static Builder ctrl() {
		return new Builder().ctrl();
	}
	
	public static Builder alt() {
		return new Builder().alt();
	}
	
	public static Builder shift() {
		return new Builder().shift();
	}
	
	public static Builder.SpecialKeys key() {
		return new Builder().key();
	}
	
    public static Builder.SpecialKeys keyReleased() {
        return new Builder().keyReleased();
    }
	
	// Builder
	public static class Builder {
		// no modifier initially
		private int modifiersMask = 0;
		private KeyAction keyAction = KeyAction.PRESS;
		
		private Builder() {
			// intentionally left blank
		}
		
		public Builder ctrl() {
			modifiersMask |= InputEvent.CTRL_DOWN_MASK;
			return this;
		}
		
		public Builder alt() {
			modifiersMask |= InputEvent.ALT_DOWN_MASK;
			return this;
		}
		
		public Builder shift() {
			modifiersMask |= InputEvent.SHIFT_DOWN_MASK;
			return this;
		}
		
		public KeyStroke key(char key) {
			int keyCode = KeyEvent.getExtendedKeyCodeForChar(key);
			if (keyCode == KeyEvent.VK_UNDEFINED) {
				throw new IllegalArgumentException("The character '" + key + "' does not map to any virtual key.");
			}
			return keyStroke(keyCode);
		}

		public KeyStroke keyReleased(char key) {
			keyAction = KeyAction.RELEASE;
			return key(key);
		}
		
		public SpecialKeys key() {
			return new SpecialKeys();
		}

        public SpecialKeys keyReleased() {
			keyAction = KeyAction.RELEASE;
			return key();
        }
		
		private KeyStroke keyStroke(int keyCode) {
			final boolean onKeyRelease = (keyAction == KeyAction.RELEASE);
			return KeyStroke.getKeyStroke(keyCode, modifiersMask, onKeyRelease);
		}
		
		private enum KeyAction {
			PRESS, RELEASE;
		}
		
		// Special keys
		public class SpecialKeys {
			private SpecialKeys() {
				// intentionally left blank
			}
			
			public KeyStroke enter() {
				return keyStroke(KeyEvent.VK_ENTER);
			}
			
			public KeyStroke escape() {
				return keyStroke(KeyEvent.VK_ESCAPE);
			}
			
			public KeyStroke tab() {
				return keyStroke(KeyEvent.VK_TAB);
			}
			
			public KeyStroke delete() {
				return keyStroke(KeyEvent.VK_DELETE);
			}
			
			public KeyStroke insert() {
				return keyStroke(KeyEvent.VK_INSERT);
			}
			
			public KeyStroke home() {
				return keyStroke(KeyEvent.VK_HOME);
			}
			
			public KeyStroke end() {
				return keyStroke(KeyEvent.VK_END);
			}
			
			public KeyStroke pageUp() {
				return keyStroke(KeyEvent.VK_PAGE_UP);
			}
			
			public KeyStroke pageDown() {
				return keyStroke(KeyEvent.VK_PAGE_DOWN);
			}
			
			public KeyStroke pause() {
				return keyStroke(KeyEvent.VK_PAUSE);
			}
			
			// arrows
			public KeyStroke up() {
				return keyStroke(KeyEvent.VK_UP);
			}
			
			public KeyStroke down() {
				return keyStroke(KeyEvent.VK_DOWN);
			}
			
			public KeyStroke left() {
				return keyStroke(KeyEvent.VK_LEFT);
			}
			
			public KeyStroke right() {
				return keyStroke(KeyEvent.VK_RIGHT);
			}
			
			// locks
			public KeyStroke numLock() {
				return keyStroke(KeyEvent.VK_NUM_LOCK);
			}
			
			public KeyStroke capsLock() {
				return keyStroke(KeyEvent.VK_CAPS_LOCK);
			}
			
			public KeyStroke scrollLock() {
				return keyStroke(KeyEvent.VK_SCROLL_LOCK);
			}
			
			// function keys
			public KeyStroke f1() {
				return keyStroke(KeyEvent.VK_F1);
			}
			
			public KeyStroke f2() {
				return keyStroke(KeyEvent.VK_F2);
			}
			
			public KeyStroke f3() {
				return keyStroke(KeyEvent.VK_F3);
			}
			
			public KeyStroke f4() {
				return keyStroke(KeyEvent.VK_F5);
			}
			
			public KeyStroke f5() {
				return keyStroke(KeyEvent.VK_F5);
			}
			
			public KeyStroke f6() {
				return keyStroke(KeyEvent.VK_F6);
			}
			
			public KeyStroke f7() {
				return keyStroke(KeyEvent.VK_F7);
			}
			
			public KeyStroke f8() {
				return keyStroke(KeyEvent.VK_F8);
			}
			
			public KeyStroke f9() {
				return keyStroke(KeyEvent.VK_F9);
			}
			
			public KeyStroke f10() {
				return keyStroke(KeyEvent.VK_F10);
			}
			
			public KeyStroke f11() {
				return keyStroke(KeyEvent.VK_F11);
			}
			
			public KeyStroke f12() {
				return keyStroke(KeyEvent.VK_F12);
			}
			
			// numpad
			public KeyStroke numpad0() {
				return keyStroke(KeyEvent.VK_NUMPAD0);
			}

			public KeyStroke numpad1() {
				return keyStroke(KeyEvent.VK_NUMPAD1);
			}

			public KeyStroke numpad2() {
				return keyStroke(KeyEvent.VK_NUMPAD2);
			}

			public KeyStroke numpad3() {
				return keyStroke(KeyEvent.VK_NUMPAD3);
			}

			public KeyStroke numpad4() {
				return keyStroke(KeyEvent.VK_NUMPAD4);
			}

			public KeyStroke numpad5() {
				return keyStroke(KeyEvent.VK_NUMPAD5);
			}

			public KeyStroke numpad6() {
				return keyStroke(KeyEvent.VK_NUMPAD6);
			}

			public KeyStroke numpad7() {
				return keyStroke(KeyEvent.VK_NUMPAD7);
			}

			public KeyStroke numpad8() {
				return keyStroke(KeyEvent.VK_NUMPAD9);
			}

			public KeyStroke numpad9() {
				return keyStroke(KeyEvent.VK_NUMPAD9);
			}
		}
	}
}