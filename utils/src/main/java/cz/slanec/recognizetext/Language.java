package cz.slanec.recognizetext;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.io.Resources;

public enum Language {
	ENGLISH("English.txt", "EnglishTextCharFrequency.dat");
	
	private final String textFileName;
	private final String charFreqDataFileName;
	private final ImmutableMultiset<Character> charFreq;
	
	private Language(String textFileName, String charFreqDataFileName) {
		this.textFileName = textFileName;
		this.charFreqDataFileName = charFreqDataFileName;
		this.charFreq = readCharFreqDataFromFile();
	}
	
	/**
	 * @return TODO
	 */
	public double getFitness(CharSequence input) {
		Multiset<Character> inputCharFreq = countCharFrequency(input);
		double difference = charFreqDifferenceFromText(inputCharFreq);
		return difference / input.length();
	}
	
	/**
	 * @param strings
	 *        An {@link Iterable} of {@code Strings} to inspect.
	 * @return the {@code String} which looks the most like a proper English text 
	 */
	public <T extends CharSequence> T recognizeFrom(Iterable<T> strings) {
		int minDifference = Integer.MAX_VALUE;
		T bestCandidate = null;
		for (T candidate : strings) {
			Multiset<Character> chars = countCharFrequency(candidate);
			int difference = charFreqDifferenceFromText(chars);
			if (minDifference > difference) {
				minDifference = difference;
				bestCandidate = candidate;
			}
		}
		return bestCandidate;
	}
	
	/**
	 * @return A {@link Multiset} of found characters from char to its occurence
	 *         count in the String.
	 */
	private static ImmutableMultiset<Character> countCharFrequency(CharSequence input) {
		ImmutableMultiset.Builder<Character> chars = ImmutableMultiset.builder();
		for (int i = 0; i < input.length(); i++) {
			chars.add(input.charAt(i));
		}
		return chars.build();
	}

	/**
	 * Compares the supplied character counts to the expected counts learnt from
	 * real text.
	 * @return A score representing the difference between the supplied text and
	 *         the real text.
	 */
	private int charFreqDifferenceFromText(Multiset<Character> chars) {
		int difference = 0;
		int charactersCounted = 0;
		final int charsSize = chars.size();
		final int langTextSize = charFreq.size();
		for (Entry<Character> entry : charFreq.entrySet()) {
			Character character = entry.getElement();
			int expectedCharacterCount = entry.getCount() * charsSize / langTextSize;
			int actualCharacterCount = chars.count(character);
			difference += Math.abs(expectedCharacterCount - actualCharacterCount);
			charactersCounted += actualCharacterCount;
		}
		// add all remaining characters to the difference
		difference += chars.size() - charactersCounted;
		return difference;
	}
	
	/**
	 * Loads the txt file with the real EN text, learns its contents and writes
	 * them out serialized.
	 * @return The learnt character counts.
	 */
	private ImmutableMultiset<Character> writeCharFreqDataToFile() {
		// TODO split into two methods
		// load the file and learn its contents
		String text;
		try {
			text = Resources.toString(Language.class.getResource(textFileName), Charsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		ImmutableMultiset<Character> freq = countCharFrequency(text);
		
		// serialize it out
		Path output = Paths.get(charFreqDataFileName);
		try (ObjectOutput out = new ObjectOutputStream(Files.newOutputStream(output))) {
			out.writeObject(freq);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return freq;
	}

	/** Reads the serialized learnt data from file. */
	@SuppressWarnings("unchecked")	// deserialization cast, guaranteed to be ok
	private ImmutableMultiset<Character> readCharFreqDataFromFile() {
		try (ObjectInput in = new ObjectInputStream(Language.class.getResourceAsStream(charFreqDataFileName))) {
			Object o = in.readObject();
			if (o instanceof ImmutableMultiset<?>) {
				return (ImmutableMultiset<Character>)o;
			}
		} catch (IOException | ClassNotFoundException | NullPointerException e) {
			// nothing to do
		}
		// fall back to txt file
		return writeCharFreqDataToFile();
	}

}