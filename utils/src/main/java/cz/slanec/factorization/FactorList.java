package cz.slanec.factorization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.common.base.Joiner;

class FactorList extends ArrayList<Factor> {
	private static final Joiner FACTOR_JOINER = Joiner.on(" * ");

	/**
	 * @param o
	 *        A {@link Factor} instance or an integer number.
	 * @return <code>true</code> if this list contains the exact Factor or any Factor
	 *         instance with the {@code factor} field equal to the integer provided
	 */
	@Override
	public boolean contains(Object o) {
		if (o instanceof Factor) {
			return (Collections.binarySearch(this, (Factor)o) >= 0);
		}
		if (o instanceof Number) {
			return (Collections.binarySearch(this, (Number)o, FactorNumberComparator.INSTANCE) >= 0);
		}
		return false;
	}
	
	/** Returns a nice string representation of the factorization, e.g. -60 factorized returns {@code -1 * 2^2 * 3 * 5} */
	@Override
	public String toString() {
		return FACTOR_JOINER.join(this);
	}
	
	/**
	 * Comparator for deciding whether a factorization contains a specific
	 * integer in it. Takes only the Factors' {@code factor} field into account.
	 * <p>
	 * Note: this comparator imposes orderings that are inconsistent with equals.
	 */
	private enum FactorNumberComparator implements Comparator<Number> {
		INSTANCE;
		
		@Override
		public int compare(Number origFactor, Number intNumber) {
			final long factor = ((Factor)origFactor).factor;
			final long num = intNumber.longValue();
			return Long.compare(factor, num);
		}
	}
}