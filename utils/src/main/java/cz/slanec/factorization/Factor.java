package cz.slanec.factorization;

import java.util.Objects;

import cz.slanec.utils.Commons;

public class Factor extends Number implements Comparable<Factor> {
	public final long factor;
	public final int exponent;
	
	public Factor(long factor) {
		this(factor, 1);
	}
	
	public Factor(long factor, int exponent) {
		this.factor = factor;
		this.exponent = exponent;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Factor)) {
			return false;
		}
		Factor other = (Factor)obj;
		return (factor == other.factor) && (exponent == other.exponent);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(factor, exponent);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(factor);
		if (exponent > 1) {
			sb.append('^');
			sb.append(exponent);
		}
		return sb.toString();
	}


	@Override
	public int compareTo(Factor f) {
		int signum = Long.signum(factor - f.factor);
		return (signum == 0) ? Integer.compare(exponent, f.exponent) : signum;
	}

	@Override
	public int intValue() {
		return (int)longValue();
	}

	@Override
	public long longValue() {
		return Commons.powLong(factor, exponent);
	}

	@Override
	public float floatValue() {
		return longValue();
	}

	@Override
	public double doubleValue() {
		return longValue();
	}
}