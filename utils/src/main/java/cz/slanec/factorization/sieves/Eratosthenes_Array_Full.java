package cz.slanec.factorization.sieves;

public class Eratosthenes_Array_Full implements Sieve {
	private final boolean[] sieve;
	private final int size;
	
	public Eratosthenes_Array_Full() {
		this(1<<24);	// 16 777 216
	}
	
	public Eratosthenes_Array_Full(int size) {
		if ((size < 0) || (size > Integer.MAX_VALUE - 8)) {
			throw new IllegalArgumentException("Illegal sieve size!");
		}
		this.size = size;
		sieve = new boolean[size+1];
		
		sieve[0] = true;	// 0 is not prime
		sieve[1] = true;	// 1 is not prime
		for (int currentPrime = 2; currentPrime < size; currentPrime++) {
			// skip composites
			if (sieve[currentPrime]) {
				continue;
			}
			// prime found, mark all it's multiplies as composites
			for (int i = 2*currentPrime; (i > 0) && (i < size); i += currentPrime) {
				sieve[i] = true;
			}
		}
	}

	@Override
	public boolean isPrime(int number) {
		number = Math.abs(number);
		if (number > size) {
			throw new IllegalArgumentException("The underlying sieve can't resolve "
					+ number + ". Instantiate the Sieve with a larger capacity.");
		}
		return !sieve[number];
	}
	
	@Override
	public int nextPrime(int number) {
		number = Math.abs(number) + 1;
		if (number > size) {
			throw new IllegalArgumentException("The underlying sieve can't resolve "
					+ number + ". Instantiate the Sieve with a larger capacity.");
		}
		while (number < size) {
			if (!sieve[number]) {
				return number;
			}
			number++;
		}
		return -1;
	}
	
	@Override
	public int previousPrime(int number) {
		number = Math.abs(number) - 1;
		if (number > size) {
			throw new IllegalArgumentException("The underlying sieve can't resolve "
					+ number + ". Instantiate the Sieve with a larger capacity.");
		}
		while (number > 0) {
			if (!sieve[number]) {
				return number;
			}
			number--;
		}
		return -1;
	}
}