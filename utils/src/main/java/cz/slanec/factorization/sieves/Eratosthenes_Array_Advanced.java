package cz.slanec.factorization.sieves;

public class Eratosthenes_Array_Advanced extends AbstractTwoSieve {
	private final boolean[] sieve;
	
	public Eratosthenes_Array_Advanced() {
		this(1<<24);	// 16 777 216
	}
	
	public Eratosthenes_Array_Advanced(int maxNumber) {
		if ((maxNumber < 0) || (maxNumber > Integer.MAX_VALUE - 8)) {
			throw new IllegalArgumentException("Illegal sieve size!");
		}
		this.maxNumber = maxNumber;

		// only store 8 values for every 30 numbers
		size = (maxNumber/30 + 1) * 8;
		sieve = new boolean[size];
		
		sieve[numberToIndex(1)] = true;	// 1 is not a prime
		for (int currentIndex = 1; currentIndex < size; currentIndex++) {
			// skip composites
			if (sieve[currentIndex]) {
				continue;
			}
			// prime found, mark all it's multiplies as composites
			final int prime = indexToNumber(currentIndex);
			for (int composite = 2*prime; composite < maxNumber; composite += prime) {
				sieve[numberToIndex(composite)] = true;
			}
		}
	}

	@Override
	public boolean isPrime(int number) {
		number = checkNumber(number);
		if (number < 2) {
			return false;
		}
		if (number == 2 || number == 3 || number == 5) {
			return true;
		}
		if ((number % 2 == 0) || (number % 3 == 0) || (number % 5 == 0)) {
			return false;
		}
		return !sieve[numberToIndex(number)];
	}
	
	@Override
	public int nextPrime(int number) {
		number = checkNumber(number);
		if (number < 2) {
			return 2;
		}
		if (number == 2) {
			return 3;
		}
		if (number < 5) {
			return 5;
		}
		int index = numberToIndex(number+1);
		while (index == 0) {
			number++;
			index = numberToIndex(number);
		}
		while (index < size) {
			if (!sieve[index]) {
				return indexToNumber(index);
			}
			index++;
		}
		return -1;
	}
	
	@Override
	public int previousPrime(int number) {
		number = checkNumber(number);
		if (number <= 2) {
			return -1;
		}
		if (number == 3) {
			return 2;
		}
		if (number <= 5) {
			return 3;
		}
		if (number <= 7) {
			return 5;
		}
		int index = numberToIndex(number) - 1;
		while (index <= 0 && number > 7) {
			number--;
			index = numberToIndex(number);
		}
		while (index > 0) {
			if (!sieve[index]) {
				return indexToNumber(index);
			}
			index--;
		}
		return -1;
	}
	
	static int numberToIndex(int number) {
		final int line = number / 30 * 8;
		switch (number % 30) {
			case  1: return line + 0;
			case  7: return line + 1;
			case 11: return line + 2;
			case 13: return line + 3;
			case 17: return line + 4;
			case 19: return line + 5;
			case 23: return line + 6;
			case 29: return line + 7;
			default: return 0;
		}
	}
	
	static int indexToNumber(int index) {
		final int line = index / 8 * 30;
		switch (index % 8) {
			case 0: return line +  1;
			case 1: return line +  7;
			case 2: return line + 11;
			case 3: return line + 13;
			case 4: return line + 17;
			case 5: return line + 19;
			case 6: return line + 23;
			case 7: return line + 29;
			default: throw new IllegalArgumentException("Wrong index.");
		}
	}
}