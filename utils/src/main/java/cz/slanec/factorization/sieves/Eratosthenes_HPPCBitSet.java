package cz.slanec.factorization.sieves;

import com.carrotsearch.hppc.BitSet;

public class Eratosthenes_HPPCBitSet extends AbstractTwoSieve {
	private final BitSet sieve;
	
	public Eratosthenes_HPPCBitSet() {
		this(1<<24);	// 16 777 216
	}

	public Eratosthenes_HPPCBitSet(int maxNumber) {
		if (maxNumber < 0) {
			throw new IllegalArgumentException("Illegal sieve size!");
		}
		this.maxNumber = maxNumber;

		// only store values for odd numbers
		size = maxNumber/2 + (maxNumber % 2);
		sieve = new BitSet(size);
		
		sieve.set(0);	// 1 is not prime
		for (int currentIndex = 1; currentIndex < size; currentIndex++) {
			// skip composites
			if (sieve.get(currentIndex)) {
				continue;
			}
			// prime found, mark all it's multiplies as composites
			final int prime = indexToNumber(currentIndex);
			for (int j = currentIndex + prime; (j > 0) && (j < size); j += prime) {
				sieve.set(j);
			}
		}
	}

	@Override
	public boolean isPrime(int number) {
		number = checkNumber(number);
		if (number == 2) {
			return true;
		}
		if (number % 2 == 0) {
			return false;
		}
		return !sieve.get(numberToIndex(number));
	}
	
	@Override
	public int nextPrime(int number) {
		number = checkNumber(number);
		if (number < 2) {
			return 2;
		}
		for (int index = numberToIndex(number+1); index < size; index++) {
			if (!sieve.get(index)) {
				return indexToNumber(index);
			}
		}
		return -1;
	}
	
	@Override
	public int previousPrime(int number) {
		number = checkNumber(number);
		if (number <= 2) {
			return -1;
		}
		if (number == 3) {
			return 2;
		}
		for (int index = numberToIndex(number) - 1; index > 0; index--) {
			if (!sieve.get(index)) {
				return indexToNumber(index);
			}
		}
		return -1;
	}
}