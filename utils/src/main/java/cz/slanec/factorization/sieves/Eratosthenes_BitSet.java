package cz.slanec.factorization.sieves;

import java.util.BitSet;

public class Eratosthenes_BitSet extends AbstractTwoSieve {
	private final BitSet sieve;
	
	public Eratosthenes_BitSet() {
		this(1<<24);	// 16 777 216
	}

	public Eratosthenes_BitSet(int maxNumber) {
		if (maxNumber < 0) {
			throw new IllegalArgumentException("Illegal sieve size!");
		}
		this.maxNumber = maxNumber;

		// only store values for odd numbers
		size = maxNumber/2 + (maxNumber % 2);
		sieve = new BitSet(size);
		
		sieve.set(0);	// 1 is not prime
		for (int currentIndex = 1; currentIndex < size; currentIndex = sieve.nextClearBit(++currentIndex)) {
			// prime found, mark all it's multiplies as composites
			final int prime = indexToNumber(currentIndex);
			for (int j = currentIndex + prime; (j > 0) && (j < size); j += prime) {
				sieve.set(j);
			}
		}
	}

	@Override
	public boolean isPrime(int number) {
		number = checkNumber(number);
		if (number == 2) {
			return true;
		}
		if (number % 2 == 0) {
			return false;
		}
		return !sieve.get(numberToIndex(number));
	}
	
	@Override
	public int nextPrime(int number) {
		number = checkNumber(number);
		if (number < 2) {
			return 2;
		}
		return indexToNumber(sieve.nextClearBit(numberToIndex(number+1)));
	}
	
	@Override
	public int previousPrime(int number) {
		number = checkNumber(number);
		if (number <= 2) {
			return -1;
		}
		if (number == 3) {
			return 2;
		}
		return indexToNumber(sieve.previousClearBit(numberToIndex(number) - 1));
	}
}