package cz.slanec.factorization.sieves;

import static com.google.common.base.Preconditions.checkArgument;

abstract class AbstractTwoSieve implements Sieve {
	int maxNumber;
	int size;
	
	protected int checkNumber(int number) {
		// TODO: handle Integer.MIN_VALUE
		number = Math.abs(number);
		checkArgument(number <= maxNumber,
				"The underlying sieve can't resolve %s. Instantiate the Sieve with a larger capacity.", number);
		return number;
	}
	
	static int numberToIndex(int number) {
		return (number)/2;
	}
	
	static int indexToNumber(int index) {
		return (2*index + 1);
	}
}