package cz.slanec.factorization.sieves;

public interface Sieve {
	boolean isPrime(int number);
	int nextPrime(int number);
	int previousPrime(int number);
	// TODO nth prime
	// TODO create sieves on thy fly
}