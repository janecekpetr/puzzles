package cz.slanec.factorization.sieves;

public class Eratosthenes_Array extends AbstractTwoSieve {
	private final boolean[] sieve;
	
	public Eratosthenes_Array() {
		this(1<<24);	// 16 777 216
	}
	
	public Eratosthenes_Array(int maxNumber) {
		if ((maxNumber < 0) || (maxNumber > Integer.MAX_VALUE - 8)) {
			throw new IllegalArgumentException("Illegal sieve size!");
		}
		this.maxNumber = maxNumber;

		// only store values for odd numbers
		size = maxNumber/2 + (maxNumber % 2);
		sieve = new boolean[size];
		
		sieve[0] = true;	// 1 is not prime
		for (int currentIndex = 1; currentIndex < size; currentIndex++) {
			// skip composites
			if (sieve[currentIndex]) {
				continue;
			}
			// prime found, mark all it's multiplies as composites
			final int prime = indexToNumber(currentIndex);
			for (int i = currentIndex + prime; i < size; i += prime) {
				sieve[i] = true;
			}
		}
	}

	@Override
	public boolean isPrime(int number) {
		number = checkNumber(number);
		if (number == 2) {
			return true;
		}
		if (number % 2 == 0) {
			return false;
		}
		return !sieve[numberToIndex(number)];
	}
	
	@Override
	public int nextPrime(int number) {
		number = checkNumber(number);
		if (number < 2) {
			return 2;
		}
		for (int index = numberToIndex(number+1); index < size; index++) {
			if (!sieve[index]) {
				return indexToNumber(index);
			}
		}
		return -1;
	}
	
	@Override
	public int previousPrime(int number) {
		number = checkNumber(number);
		if (number <= 2) {
			return -1;
		}
		if (number == 3) {
			return 2;
		}
		for (int index = numberToIndex(number) - 1; index > 0; index--) {
			if (!sieve[index]) {
				return indexToNumber(index);
			}
		}
		return -1;
	}
}