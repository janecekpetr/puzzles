package cz.slanec.factorization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.slanec.utils.Commons;

public class Factorization {
	
	public static long valueOf(List<Factor> factors) {
		long result = 1;
		for (Factor factor : factors) {
			result *= Commons.powLong(factor.factor, factor.exponent);
		}
		return result;
	}
	
	/** Returns a list of proper divisors of the argument. */
	public static List<Long> divisors(long number) {
		return divisors(factorize(number));
	}
	
	/** Returns a sorted list of proper divisors of the argument extracted from the factors. */
	public static List<Long> divisors(Iterable<? extends Factor> factors) {
		List<Long> divisors = new ArrayList<>();
		divisors.add(1L);
		
		for (Factor f : factors) {
			List<Long> temp = new ArrayList<>();
			for (int exp = 1; exp <= f.exponent; exp++) {
				long factor = Commons.powLong(f.factor, exp);
				for (long divisor : divisors) {
					temp.add(divisor * factor);
				}
			}
			divisors.addAll(temp);
		}
		
		Collections.sort(divisors);
		divisors.remove(divisors.size() - 1);	// the original number itself
		return divisors;
	}
	
	/**
	 * Returns <code>true</code> iff the argument is a prime. Suitable for one-time-use.
	 * If you want to test many numbers, use a Sieve.
	 */
	public static boolean isPrime(long number) {
		if (number < 2) {
			return false;
		}
		if (number == 2 || number == 3 || number == 5) {
			return true;
		}
		if ((number % 2 == 0) || (number % 3 == 0) || (number % 5 == 0)) {
			return false;
		}
		long max = (long)Math.sqrt(number);
		for (long i = 0; i <= max; i += 30) {
			if (		number % (i +  7) == 0
					|| (number % (i + 11) == 0)
					|| (number % (i + 13) == 0)
					|| (number % (i + 17) == 0)
					|| (number % (i + 19) == 0)
					|| (number % (i + 23) == 0)
					|| (number % (i + 29) == 0)
					|| (number % (i + 31) == 0) ) {
				return (number <= i+31);
			}
		}
		return true;
	}
	
	public static List<Factor> factorize(long number) {
		return new Factorization(number).factorize();
	}

	/* factorization itself */
	private final List<Factor> factors = new FactorList();
	private long number;
	
	private Factorization(long number) {
		this.number = number;
	}
	
	private List<Factor> factorize() {
		if (number == 0 || number == 1) {
			factors.add(new Factor(number));
			return factors;
		}
		if (number < 0) {
			factors.add(new Factor(-1));
			number = -number;
		}
		divide(2);
		divide(3);
		divide(5);
		for (long i = 0; i*i <= number; i += 30) {
			divide(i+ 7);
			divide(i+11);
			divide(i+13);
			divide(i+17);
			divide(i+19);
			divide(i+23);
			divide(i+29);
			divide(i+31);
		}
		if (number > 1) {
			factors.add(new Factor(number));
		}
		return factors;
	}
	
	private void divide(long divisor) {
		int exponent = 0;
		while (number % divisor == 0) {
			number /= divisor;
			exponent++;
		}
		if (exponent > 0) {
			factors.add(new Factor(divisor, exponent));
		}
	}
}