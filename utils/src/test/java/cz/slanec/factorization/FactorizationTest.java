package cz.slanec.factorization;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.google.common.math.LongMath;

public class FactorizationTest {
	
	public static long getNumber() {
		try (Scanner scan = new Scanner(System.in)) {
			System.out.print("Gimme a numba: ");
			if (!scan.hasNextLong()) {
				throw new InputMismatchException("No longint found!");
			}
			return scan.nextLong();
		}
	}
	
	public static void main(String[] args) {
//		922337203685477647
//		14640273074372677
		long num = getNumber();
		
		long time = System.currentTimeMillis();
		System.out.println(Factorization.isPrime(num));
		System.out.println("time: " + (System.currentTimeMillis() - time));
		
		time = System.currentTimeMillis();
		System.out.println(LongMath.isPrime(num));
		System.out.println("time: " + (System.currentTimeMillis() - time));
	}

}