package cz.slanec.factorization.sieves;

public class SieveTest {
	
	public static void main(String[] args) {
		testSieves();
		
		Sieve s1 = new Eratosthenes_Array_Full();
		Sieve s2 = new Eratosthenes_Array();
		Sieve s3 = new Eratosthenes_Array_Advanced();
		Sieve s4 = new Eratosthenes_BitSet();
		Sieve s5 = new Eratosthenes_HPPCBitSet();
		for (int i = 0; i < 10_000_000; i++) {
			if (s1.previousPrime(i) != s2.previousPrime(i)
					|| s2.previousPrime(i) != s3.previousPrime(i)
					|| s3.previousPrime(i) != s4.previousPrime(i)
					|| s4.previousPrime(i) != s5.previousPrime(i)) {
				System.out.println(i);
				System.out.println();
				System.out.println(s1.previousPrime(i));
				System.out.println(s2.previousPrime(i));
				System.out.println(s3.previousPrime(i));
				System.out.println(s4.previousPrime(i));
				System.out.println(s5.previousPrime(i));
				break;
			}
		}
	}
	
	public static void testSieves() {
		int max = Integer.MAX_VALUE/10;
		long time;
		
		time = System.currentTimeMillis();
		testSieve(new Eratosthenes_Array_Full(max));
		System.out.println(System.currentTimeMillis() - time);

		time = System.currentTimeMillis();
		testSieve(new Eratosthenes_Array(max));
		System.out.println(System.currentTimeMillis() - time);
		
		time = System.currentTimeMillis();
		testSieve(new Eratosthenes_BitSet(max));
		System.out.println(System.currentTimeMillis() - time);
		
		time = System.currentTimeMillis();
		testSieve(new Eratosthenes_HPPCBitSet(max));
		System.out.println(System.currentTimeMillis() - time);
	}
	
	public static void testSieve(Sieve s) {
		System.out.println(s.getClass().getSimpleName());

		for (int i = 0; i < 10; i++) {
			System.out.print(i + ": " + s.nextPrime(i) + ", ");
		}
		System.out.println();
		
//		for (int i = 0; i < 10; i++) {
//			System.out.print(i + ": " + s.getPreviousPrime(i) + ", ");
//		}
//		System.out.println();
		
		
//		for (int i = 0; i < 38; i++) {
//			if (s.isPrime(i)) {
//				System.out.print(i + ", ");
//			}
//		}
//		System.out.println();
	}

}