package cz.slanec.gui;

import static org.junit.Assert.assertEquals;

import javax.swing.KeyStroke;

import org.junit.Test;

public class KeyStrokesTest {
	@Test
	public void shortcutsTest() {
		assertEquals(KeyStroke.getKeyStroke("alt pressed A"), KeyStrokes.alt('a'));
	}
}