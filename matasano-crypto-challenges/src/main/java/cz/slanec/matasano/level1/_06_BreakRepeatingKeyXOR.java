package cz.slanec.matasano.level1;

import static org.junit.Assert.assertEquals;

import java.nio.file.Paths;

import cz.slanec.matasano.DecryptInfo;
import cz.slanec.matasano.Matasanos;

/**
 * 6. Break repeating-key XOR
 * <p>
 * The buffer at the following location:
 * <a href="https://gist.github.com/3132752">https://gist.github.com/3132752</a>
 * is base64-encoded repeating-key XOR. Break it.
 * <p>
 * Here's how:
 * <ol>
 * <li>Let KEYSIZE be the guessed length of the key; try values from 2 to
 * (say) 40.</li>
 * <li>Write a function to compute the edit distance/Hamming distance
 * between two strings. The Hamming distance is just the number of
 * differing bits. The distance between: "<code>this is a test</code>" and:
 * "<code>wokka wokka!!!</code>" is 37.</li>
 * <li>For each KEYSIZE, take the FIRST KEYSIZE worth of bytes, and the
 * SECOND KEYSIZE worth of bytes, and find the edit distance between
 * them. Normalize this result by dividing by KEYSIZE.</li>
 * <li>The KEYSIZE with the smallest normalized edit distance is probably
 * the key. You could proceed perhaps with the smallest 2-3 KEYSIZE
 * values. Or take 4 KEYSIZE blocks instead of 2 and average the
 * distances.</li>
 * <li>Now that you probably know the KEYSIZE: break the ciphertext into
 * blocks of KEYSIZE length.</li>
 * <li>Now transpose the blocks: make a block that is the first byte of
 * every block, and a block that is the second byte of every block, and
 * so on.</li>
 * <li>Solve each block as if it was single-character XOR. You already
 * have code to do this.</li>
 * <li>For each block, the single-byte XOR key that produces the best
 * looking histogram is the repeating-key XOR key byte for that
 * block. Put them together and you have the key.</li>
 * </ol>
 */
public class _06_BreakRepeatingKeyXOR {
	
	public static void main(String[] args) {
		byte[] encrypted = Matasanos.parseBase64File(Paths.get("files/_06_BreakRepeatingKeyXOR.txt"));
		
		DecryptInfo decryptInfo = Matasanos.breakRepeatingKeyXOR(encrypted);
		System.out.println(decryptInfo.key);
		assertEquals("Terminator X: Bring the noise", decryptInfo.key);
	}
	
}