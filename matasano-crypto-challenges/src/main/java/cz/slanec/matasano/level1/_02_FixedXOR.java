package cz.slanec.matasano.level1;

import static cz.slanec.utils.Commons.xor;
import static org.junit.Assert.assertEquals;

import cz.slanec.utils.Strings;

/**
 * 2. Fixed XOR
 * <p>
 * Write a function that takes two equal-length buffers and produces their XOR sum.
 * <p>
 * The string: <code>1c0111001f010100061a024b53535009181c</code><br />
 * ...after hex decoding, when xor'd against: 
 * <code>686974207468652062756c6c277320657965</code><br />
 * ...should produce: <code>746865206b696420646f6e277420706c6179</code>
 */
public class _02_FixedXOR {
	private static final String HEX = "1c0111001f010100061a024b53535009181c";
	private static final String XOR = "686974207468652062756c6c277320657965";
	private static final String EXPECTED = "746865206b696420646f6e277420706c6179";
	
	public static void main(String[] args) {
		byte[] raw = Strings.parseHexString(HEX);
		byte[] key = Strings.parseHexString(XOR);
		byte[] rawXored = xor(raw, key);
		System.out.println(Strings.of(rawXored));
		
		String result = Strings.toHexString(rawXored);
		System.out.println(result);
		assertEquals(EXPECTED, result);
	}
}