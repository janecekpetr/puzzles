package cz.slanec.matasano.level1;

import static org.junit.Assert.assertEquals;

import java.util.Base64;

import cz.slanec.utils.Strings;

/**
 * 1. Convert hex to base64 and back.
 * <p>
 * The string:<br />
 * <code>49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6
 * e6f7573206d757368726f6f6d</code><br />
 * should produce:<br />
 * <code>SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t</code>
 * <p>
 * Now use this code everywhere for the rest of the exercises. Here's a simple
 * rule of thumb:<br />
 * Always operate on raw bytes, never on encoded strings. Only use hex and base64
 * for pretty-printing.
 */
public class _01_HexToBase64AndBack {
	private static final String HEX =
			"49276d206b696c6c696e6720796f757220627261696e206c6"
			+ "96b65206120706f69736f6e6f7573206d757368726f6f6d";
	private static final String EXPECTED =
			"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
	
	public static void main(String[] args) {
		byte[] raw = Strings.parseHexString(HEX);
		System.out.println(Strings.of(raw));
		
		String base64 = Base64.getEncoder().encodeToString(raw);
		System.out.println(base64);
		
		assertEquals(EXPECTED, base64);
	}
}