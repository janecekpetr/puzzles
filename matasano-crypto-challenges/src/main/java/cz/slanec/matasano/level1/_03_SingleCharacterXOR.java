package cz.slanec.matasano.level1;

import static org.junit.Assert.assertEquals;

import cz.slanec.matasano.DecryptInfo;
import cz.slanec.matasano.Matasanos;
import cz.slanec.utils.Strings;

/**
 * 3. Single-character XOR Cipher
 * <p>
 * The hex encoded string:
 * <code>1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736</code><br />
 * ...has been XOR'd against a single character. Find the key, decrypt the message.
 * <p>
 * Write code to do this for you. How? Devise some method for "scoring" a
 * piece of English plaintext. (Character frequency is a good metric.)
 * <p>
 * Evaluate each output and choose the one with the best score. Tune your algorithm
 * until this works.
 */
public class _03_SingleCharacterXOR {
	private static final String ENCRYPTED_HEX =
			"1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
	
	public static void main(String[] args) {
		byte[] encrypted = Strings.parseHexString(ENCRYPTED_HEX);
		
		DecryptInfo decryptInfo = Matasanos.decryptSingleCharXor(encrypted);
		
		System.out.println(decryptInfo.decrypted);
		System.out.println("Key was: " + decryptInfo.key);
		assertEquals("Cooking MC's like a pound of bacon", decryptInfo.decrypted);
	}
}