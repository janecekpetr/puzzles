package cz.slanec.matasano.level1;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import cz.slanec.matasano.Matasanos;
import cz.slanec.utils.Strings;

import com.google.common.io.Files;

/**
 * 7. AES in ECB Mode
 * <p>
 * The Base64-encoded content at the following location:
 * <a href="https://gist.github.com/3132853">https://gist.github.com/3132853</a>
 * has been encrypted via AES-128 in ECB mode under the key
 * "<code>YELLOW SUBMARINE</code>".
 * (I like "<code>YELLOW SUBMARINE</code>" because it's exactly 16 bytes long).
 * <p>
 * Decrypt it.
 * <p>
 * Easiest way: Use OpenSSL::Cipher and give it AES-128-ECB as the cipher.
 */
public class _07_AESinECBMode {
	private static final String KEY = "YELLOW SUBMARINE";
	private static final String EXPECTED = loadExpected();
	
	public static void main(String[] args) {
		byte[] encrypted = Matasanos.parseBase64File(Paths.get("files/_07_AESInECBMode.txt"));
		
		String decrypted;
		try {
			byte[] keyRaw = KEY.getBytes(StandardCharsets.UTF_8);
			Key key = new SecretKeySpec(keyRaw, "AES");
			
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key);
			
			byte[] decryptedRaw = cipher.doFinal(encrypted);
			decrypted = Strings.of(decryptedRaw);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException(e);
		}
		
		System.out.println(decrypted);
		assertEquals(EXPECTED, decrypted);
	}
	
	private static String loadExpected() {
		try {
			return Files.toString(new File("files/_07_AESInECBMode_expected.txt"), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}