package cz.slanec.matasano.level1;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.slanec.utils.Strings;

/**
 * 8. Detecting ECB
 * <p>
 * At the following URL are a bunch of hex-encoded ciphertexts:
 * <a href="https://gist.github.com/3132928">https://gist.github.com/3132928</a>.
 * <p>
 * One of them is ECB encrypted. Detect it.
 * <p>
 * Remember that the problem with ECB is that it is stateless and
 * deterministic; the same 16 byte plaintext block will always produce
 * the same 16 byte ciphertext.
 */
public class _08_DetectingECB {
	
	public static void main(String[] args) {
		List<byte[]> encrypteds = new ArrayList<>();
		try (BufferedReader reader = Files.newBufferedReader(Paths.get("files/_08_DetectingECB.txt"))) {
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				encrypteds.add(Strings.parseHexString(line));
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		for (int lineNum = 1; lineNum <= encrypteds.size(); lineNum++) {
			byte[] encrypted = encrypteds.get(lineNum-1);
			if (hasRecurringEcbBlock(encrypted)) {
				System.out.println("Found a recurring pattern on line: " + lineNum);
				assertEquals(133, lineNum);
			}
		}
	}

	private static boolean hasRecurringEcbBlock(byte[] encrypted) {
		final int blockSize = 16;
		Set<Integer> blocks = new HashSet<>();
		for (int i = 0; i < encrypted.length; i += blockSize) {
			byte[] block = Arrays.copyOfRange(encrypted, i, i+blockSize);
			Integer hashcode = Arrays.hashCode(block);
			if (!blocks.add(hashcode)) {
				return true;
			}
		}
		return false;
	}

}