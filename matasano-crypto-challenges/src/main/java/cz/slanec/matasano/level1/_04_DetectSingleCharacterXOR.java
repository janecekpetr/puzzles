package cz.slanec.matasano.level1;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import cz.slanec.matasano.DecryptInfo;
import cz.slanec.matasano.Matasanos;
import cz.slanec.recognizetext.Language;
import cz.slanec.utils.Strings;

/**
 * 4. Detect single-character XOR
 * <p>
 * One of the 60-character strings at:
 * <a href="https://gist.github.com/3132713">https://gist.github.com/3132713</a>
 * has been encrypted by single-character XOR.
 * <p>
 * Find it. (Your code from #3 should help.)
 */
public class _04_DetectSingleCharacterXOR {
	
	public static void main(String[] args) {
		List<String> decryptedCandidates = new ArrayList<>();
		try (BufferedReader reader = Files.newBufferedReader(Paths.get("files/_04_DetectSingleCharacterXOR.txt"))) {
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				byte[] encrypted = Strings.parseHexString(line);
				DecryptInfo decryptInfo = Matasanos.decryptSingleCharXor(encrypted);
				if (decryptInfo != null) {
					decryptedCandidates.add(decryptInfo.decrypted);
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		String decrypted = Language.ENGLISH.recognizeFrom(decryptedCandidates);
		
		System.out.println(decrypted);
		assertEquals("Now that the party is jumping\n", decrypted);
	}
}