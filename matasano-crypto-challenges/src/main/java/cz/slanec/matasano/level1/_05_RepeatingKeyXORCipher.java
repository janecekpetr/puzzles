package cz.slanec.matasano.level1;

import static cz.slanec.utils.Commons.xor;
import static org.junit.Assert.assertEquals;

import java.nio.charset.StandardCharsets;

import cz.slanec.utils.Strings;

/**
 * 5. Repeating-key XOR Cipher
 * <p>
 * Write the code to encrypt the string:<br />
 * <code>Burning 'em, if you ain't quick and nimble<br />
 * I go crazy when I hear a cymbal</code>
 * <p>
 * Under the key "<code>ICE</code>", using repeating-key XOR. It should come out to:<br />
 * <code>0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f</code>
 * <p>
 * Encrypt a bunch of stuff using your repeating-key XOR function. Get a feel for it.
 */
public class _05_RepeatingKeyXORCipher {
	
	private static final String DECRYPTED = "Burning 'em, if you ain't quick and nimble\n"
				+ "I go crazy when I hear a cymbal";
	private static final String EXPECTED = "0b3637272a2b2e63622c2e69692a23693a2a3c6"
			+ "324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a6"
			+ "53e2b2027630c692b20283165286326302e27282f";
	private static final String KEY = "ICE";
	
	public static void main(String[] args) {
		byte[] decryptedRaw = DECRYPTED.getBytes(StandardCharsets.UTF_8);
		byte[] key = KEY.getBytes(StandardCharsets.UTF_8);
		
		String encryptedHex = Strings.toHexString(xor(decryptedRaw, key));
		
		System.out.println(encryptedHex);
		assertEquals(EXPECTED, encryptedHex);
	}

}