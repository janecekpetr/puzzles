package cz.slanec.matasano.level2;

import static org.junit.Assert.assertArrayEquals;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * 9. Implement PKCS#7 padding
 * <p>
 * Pad any block to a specific block length, by appending the number of bytes
 * of padding to the end of the block. For instance, <code>"YELLOW SUBMARINE"</code>
 * padded to 20 bytes would be: <code>"YELLOW SUBMARINE\x04\x04\x04\x04"</code>
 * <p>
 * The particulars of this algorithm are easy to find online.
 */
public class _09_ImplementPKCS7 {
	private static final byte[] EXPECTED = {89, 69, 76, 76, 79, 87, 32, 83, 85, 66, 77, 65, 82, 73, 78, 69, 4, 4, 4, 4};
	
	public static void main(String[] args) {
		byte[] raw = "YELLOW SUBMARINE".getBytes(StandardCharsets.UTF_8);
		System.out.println(Arrays.toString(raw));
		
		byte[] rawPadded = pkcs7(raw, 20);
		System.out.println(Arrays.toString(rawPadded));
		
		assertArrayEquals(EXPECTED, rawPadded);
	}

	// PKCS#7 is only defined for bytesToAdd < 256
	public static byte[] pkcs7(byte[] orig, int padTo) {
		int origLength = orig.length;
		int bytesToAdd = padTo - (origLength % padTo);
		byte[] result = Arrays.copyOf(orig, origLength + bytesToAdd);
		Arrays.fill(result, origLength, result.length, (byte)bytesToAdd);
		return result;
	}
}