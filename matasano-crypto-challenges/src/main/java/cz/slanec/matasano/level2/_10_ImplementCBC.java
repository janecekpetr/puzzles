package cz.slanec.matasano.level2;

import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import cz.slanec.matasano.Matasanos;
import cz.slanec.utils.Commons;
import cz.slanec.utils.Strings;

import com.google.common.io.Resources;

/**
 * 10. Implement CBC Mode
 * <p>
 * In CBC mode, each ciphertext block is added to the next plaintext block
 * before the next call to the cipher core.
 * <p>
 * The first plaintext block, which has no associated previous ciphertext block,
 * is added to a "fake 0th ciphertext block" called the IV.
 * <p>
 * Implement CBC mode by hand by taking the ECB function you just wrote, making
 * it encrypt instead of decrypt (verify this by decrypting whatever you encrypt
 * to test), and using your XOR function from previous exercise.
 * <p>
 * DO NOT CHEAT AND USE OPENSSL TO DO CBC MODE, EVEN TO VERIFY YOUR RESULTS.
 * What's the point of even doing this stuff if you aren't going to learn from
 * it?
 * <p>
 * The buffer at: https://gist.github.com/3132976 is intelligible (somewhat)
 * when CBC decrypted against "YELLOW SUBMARINE" with an IV of all ASCII 0
 * (\x00\x00\x00 &c)
 */
public class _10_ImplementCBC {
	private static final String PASS = "YELLOW SUBMARINE";
	private static final byte[] IV = new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	public static void main(String[] args) throws URISyntaxException {
		byte[] encrypted = Matasanos.parseBase64File(Paths.get(Resources.getResource("_10_ImplementCBC.txt").toURI()));
		byte[] decryptedRaw = decryptAesCBC(encrypted);
		String decrypted = Strings.of(decryptedRaw);
		System.out.println(decrypted);
	}
	
	private static byte[] decryptAesCBC(byte[] encrypted) {
		byte[] passRaw = PASS.getBytes(StandardCharsets.UTF_8);
		int blockSize = passRaw.length;
		if (blockSize != IV.length) {
			throw new IllegalArgumentException("Wrong IV length, must be as long as key.");
		}
		
		Cipher cipher;
		try {
			Key key = new SecretKeySpec(passRaw, "AES");
			cipher = Cipher.getInstance("AES/ECB/NOPADDING");
			cipher.init(Cipher.DECRYPT_MODE, key);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException(e);
		}
		
		byte[] decrypted = new byte[encrypted.length];
		byte[] previousBlock = IV;
		for (int i = 0; i < encrypted.length; i += blockSize) {
			byte[] currentBlock = Arrays.copyOfRange(encrypted, i, i+blockSize);
			byte[] decryptedBlock = cipher.update(currentBlock);
			decryptedBlock = Commons.xor(decryptedBlock, previousBlock);
			System.arraycopy(decryptedBlock, 0, decrypted, i, blockSize);
			previousBlock = currentBlock;
		}
		return decrypted;
	}
}