package cz.slanec.matasano.level2;

/**
 * 11. Write an oracle function and use it to detect ECB.
 * <p>
 * Now that you have ECB and CBC working:<br />
 * Write a function to generate a random AES key; that's just 16 random bytes.<br/>
 * Write a function that encrypts data under an unknown key --- that is, a function
 * that generates a random key and encrypts under it.
 * <p>
 * The function should look like:
 * <code>encryption_oracle(your-input) => [MEANINGLESS JIBBER JABBER]</code>
 * <p>
 * Under the hood, have the function APPEND 5-10 bytes (count chosen randomly)
 * BEFORE the plaintext and 5-10 bytes AFTER the plaintext.
 * <p>
 * Now, have the function choose to encrypt under ECB 1/2 the time, and
 * under CBC the other half (just use random IVs each time for CBC). Use
 * rand(2) to decide which to use.
 * <p>
 * Now detect the block cipher mode the function is using each time.
 */
public class _11_DetectECB {
	
	public static void main(String[] args) {
		// 
	}

}