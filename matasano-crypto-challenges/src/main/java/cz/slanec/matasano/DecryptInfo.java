package cz.slanec.matasano;

/**
 * Holds information about the decryption: the decrypted String and the key
 * that was used to encrypt it.
 */
public class DecryptInfo {
	public final String decrypted;
	public final String key;
	
	DecryptInfo(String decrypted, String key) {
		this.decrypted = decrypted;
		this.key = key;
	}
}