package cz.slanec.matasano;

import static cz.slanec.utils.Commons.xor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import cz.slanec.recognizetext.Language;
import cz.slanec.utils.Commons;
import cz.slanec.utils.Strings;

import com.google.common.io.Files;

public class Matasanos {
	
	private Matasanos() {
		// nothing to do
	}
	
	public static byte[] parseBase64File(Path file) {
		byte[] raw;
		try {
			String encryptedBase64 = Files.toString(file.toFile(), StandardCharsets.UTF_8);
			raw = Base64.getDecoder().decode(encryptedBase64);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return raw;
	}

	/**
	 * Guesses the one-char key that was used to XOR-encrypt the given input array.
	 * @return a {@link DecryptInfo} instance if succeeded, {@code null} otherwise
	 */
	public static DecryptInfo decryptSingleCharXor(byte[] encrypted) {
		Map<String, Character> candidatesToKeys = new HashMap<>();
		for (byte key = Strings.PRINTABLE_CHAR_LO; key < Strings.PRINTABLE_CHAR_HI; key++) {
			byte[] decryptedCandidateRaw = xor(encrypted, key);
			String decryptedCandidate = Strings.of(decryptedCandidateRaw);
			if (Strings.isPrintable(decryptedCandidate)) {
				candidatesToKeys.put(decryptedCandidate, (char)key);
			}
		}
		if (candidatesToKeys.isEmpty()) {
			return null;
		}
		String decrypted = Language.ENGLISH.recognizeFrom(candidatesToKeys.keySet());
		String key = candidatesToKeys.get(decrypted).toString();
		return new DecryptInfo(decrypted, key);
	}
	
	public static DecryptInfo breakRepeatingKeyXOR(byte[] encrypted) {
		// find the most likely key sizes
		SortedMap<Double, Integer> distancesToKeySizes = mapDistancesToKeySizes(encrypted);
		
		// fine-tune if needed
		final double maxDistanceThreshold = 3.0;
		
		// take only those with (normalized distance on one Byte) <= threshold
		List<Integer> keySizeCandidates = new ArrayList<>();
		for (Entry<Double, Integer> entry : distancesToKeySizes.entrySet()) {
			if (entry.getKey() <= maxDistanceThreshold) {
				keySizeCandidates.add(entry.getValue());
			} else {
				break;
			}
		}
		
		// for every probable keySize, decipher the key
		List<String> keyCandidates = new ArrayList<>(keySizeCandidates.size());
		keyLoop:
		for (int keySize : keySizeCandidates) {
			StringBuilder keyCandidate = new StringBuilder();
			
			byte[][] blocks = transformToBlocksCorrespondingToKeyChars(encrypted, keySize);
			for (byte[] block : blocks) {
				DecryptInfo decryptInfo = decryptSingleCharXor(block);
				if (decryptInfo == null) {
					// the key is invalid, there's no letter that fits the key
					continue keyLoop;
				}
				keyCandidate.append(decryptInfo.key);
			}
			// all blocks had a possible solution, key generated
			keyCandidates.add(keyCandidate.toString());
		}
		String key = Language.ENGLISH.recognizeFrom(keyCandidates);
		String decrypted = Strings.of(Commons.xor(encrypted, key.getBytes(StandardCharsets.UTF_8)));
		return new DecryptInfo(decrypted, key);
	}

	/**
	 * Takes samples {@code keySize} long, counts the normalized Hamming distance
	 * between them and maps them to their respective {@code keySize}. The lowest
	 * edit distance are likely to be coming from the right key. The highest
	 * {@code keySize} tried is 50.
	 */
	private static SortedMap<Double, Integer> mapDistancesToKeySizes(byte[] encrypted) {
		final int MAX_KEYSIZE = 64;
		final int SAMPLE_COUNT = 10;
		
		SortedMap<Double, Integer> distancesToKeySizes = new TreeMap<>();
		for (int keySize = 2; keySize <= MAX_KEYSIZE; keySize++) {
			// make SAMPLE_COUNT samples keySize long
			byte[][] samples = new byte[SAMPLE_COUNT][];
			for (int i = 0; i < samples.length; i++) {
				samples[i] = Arrays.copyOfRange(encrypted, i*keySize, (i+1)*keySize);
			}
			// count the average Hamming distance normalized to one Byte
			int distance = 0;
			for (int i = 0; i < SAMPLE_COUNT - 1; i++) {
				distance += Commons.hammingDistance(samples[i], samples[i+1]);
			}
			double normalizedDistance = (double)distance /(SAMPLE_COUNT-1) /keySize;
			distancesToKeySizes.put(normalizedDistance, keySize);
		}
		return distancesToKeySizes;
	}

	/**
	 * Chops the encrypted input into {@code keySize} blocks.
	 * <p>
	 * Then it makes new {@code keySize} blocks such that the first new block
	 * that is the first byte of every old block, and the second new block that
	 * is the second byte of every old block, and so on.
	 * 
	 * @return {@code keySize} blocks where each block contains only the letters
	 *         encrypted by a single letter of the key
	 */
	private static byte[][] transformToBlocksCorrespondingToKeyChars(byte[] encrypted, int keySize) {
		byte[][] blocks = new byte[keySize][];
		
		final int blockSize = encrypted.length / keySize;
		final int plusOneBorder = encrypted.length % keySize;
		for (int i = 0; i < plusOneBorder; i++) {
			blocks[i] = new byte[blockSize + 1];
		}
		for (int i = plusOneBorder; i < keySize; i++) {
			blocks[i] = new byte[blockSize];
		}
		
		for (int i = 0; i < encrypted.length; i++) {
			blocks[i%keySize][i/keySize] = encrypted[i];
		}
		return blocks;
	}
}