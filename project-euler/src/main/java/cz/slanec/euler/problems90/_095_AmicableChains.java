package cz.slanec.euler.problems90;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.slanec.factorization.Factorization;
import cz.slanec.utils.Commons;

/**
 * The proper divisors of a number are all the divisors excluding the number itself.
 * For example, the proper divisors of 28 are 1, 2, 4, 7, and 14. As the sum of these
 * divisors is equal to 28, we call it a perfect number.
 * <p>
 * Interestingly the sum of the proper divisors of 220 is 284 and the sum
 * of the proper divisors of 284 is 220, forming a chain of two numbers. For this
 * reason, 220 and 284 are called an amicable pair.
 * <p>
 * Perhaps less well known are longer chains. For example, starting with 12496, we
 * form a chain of five numbers:
 * <p>
 * 12496 -> 14288 -> 15472 -> 14536 -> 14264 (-> 12496 -> ...)
 * <p>
 * Since this chain returns to its starting point, it is called an amicable chain.
 * <p>
 * Find the smallest member of the longest amicable chain with no element exceeding
 * one million.
 */
public class _095_AmicableChains {
	private static final int MAX = 1_000_000;
	
	public static int smallestMemberOfLongestAmicableChainWithNoElementOverAMillion() {
		List<Integer> longestChain = Collections.emptyList();

		// filters out visited numbers
		boolean[] allNumbers = new boolean[MAX+1];	// BitSet is slower by 75%
		int origNumber = 1;
		while (origNumber <= MAX) {
			while (allNumbers[origNumber++] && (origNumber <= MAX)) {
				// nothing to do
			}
			
			List<Integer> chain = new ArrayList<>();
			// builds the chain, stops when a loop is found or on a limit breach
			int currNumber = origNumber;
			while ((currNumber <= MAX) && !allNumbers[currNumber]) {
				allNumbers[currNumber] = true;
				chain.add(currNumber);
				currNumber = Commons.sum(Factorization.divisors(currNumber));
			}
			// finds the loop in the chain if there is one
			int indexOf = chain.indexOf(currNumber);
			if (indexOf > 0) {
				List<Integer> loop = chain.subList(indexOf, chain.size());
				if (loop.size() > longestChain.size()) {
					longestChain = loop;
				}
			}
		}
		
		// the first is also the smallest
		return longestChain.get(0).intValue();
	}
	
}