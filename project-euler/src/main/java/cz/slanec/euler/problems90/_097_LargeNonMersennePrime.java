package cz.slanec.euler.problems90;

import static cz.slanec.utils.Commons.powLong;

import java.math.BigInteger;

/**
 * The first known prime found to exceed one million digits was discovered in
 * 1999, and is a Mersenne prime of the form 2^6972593−1; it contains exactly
 * 2,098,960 digits. Subsequently other Mersenne primes, of the form 2^(p−1),
 * have been found which contain more digits.
 * <p>
 * However, in 2004 there was found a massive non-Mersenne prime which contains
 * 2,357,207 digits: 28433*2^7830457+1.
 * <p>
 * Find the last ten digits of this prime number.
 */
public class _097_LargeNonMersennePrime {
	
	// by hand
	public static long lastTenDigitsOfALargeMersennePrimeByHand() {
		final long tenDigits = powLong(10, 10);
		
		// power moduled by 10^10 (only last 10 digits)
		long powerOfTwo = 2;
		for (int i = 1; i < 7830457; i++) {
			powerOfTwo *= 2;
			powerOfTwo %= tenDigits;
		}
		return (28433*powerOfTwo + 1) % tenDigits;
	}

	// it's good to know your libraries
	public static long lastTenDigitsOfALargeMersennePrimeBigIntegerWay() {
		final BigInteger two = BigInteger.valueOf(2);
		final BigInteger tenDigits = BigInteger.TEN.pow(10);
		
		// the massive prime, powers moduled by 10^10
		BigInteger massivePrime =
				BigInteger.valueOf(28433)
				.multiply( two.modPow(BigInteger.valueOf(7830457), tenDigits) )
				.add(BigInteger.ONE);
		
		return massivePrime.mod(tenDigits).longValue();
	}

}