package cz.slanec.euler.problems90;

/**
 * A number chain is created by continuously adding the square of the digits in
 * a number to form a new number until it has been seen before.
 * <p>
 * For example,<br />
 * 44 -> 32 -> 13 -> 10 -> 1 -> 1<br />
 * 85 -> 89 -> 145 -> 42 -> 20 -> 4 -> 16 -> 37 -> 58 -> 89
 * <p>
 * Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop.
 * What is most amazing is that EVERY starting number will eventually arrive
 * at 1 or 89.
 * <p>
 * How many starting numbers below ten million will arrive at 89?
 */
public class _092_SquareDigitChains {

	public static int howManyNumbersBelow10MillionArriveAt89ByChainingSumsOfDigitSquares() {
		int count = 0;
		for (int start = 1; start < 10000000; start++) {
			int num = start;
			while (num != 1) {
				if (num == 89) {
					count++;
					num = 1;
				}
				num = squareDigits(num);
			}
		}
		return count;
	}
	
	private static int squareDigits(int num) {
		int sum = 0;
		while (num > 0) {
			final int digit = num % 10;
			sum += digit*digit;
			num /= 10;
		}
		return sum;
	}

}