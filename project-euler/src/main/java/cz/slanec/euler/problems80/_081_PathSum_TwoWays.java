package cz.slanec.euler.problems80;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import com.google.common.io.Resources;

/**
 * In the 5 by 5 matrix below, the minimal path sum from the top left to
 * the bottom right, by only moving to the right and down, <s>is indicated
 * in bold red and</s> is equal to 2427.<pre>
 * 131	673	234	103	 18
 * 201	 96	342	965	150
 * 630	803	746	422	111
 * 537	699	497	121	956
 * 805	732	524	 37	331</pre>
 * <p>
 * Find the minimal path sum, in http://projecteuler.net/project/matrix.txt,
 * a 31K text file containing a 80 by 80 matrix, from the top left to the bottom
 * right by only moving right and down.
 */
public class _081_PathSum_TwoWays {
	private static final int matrixWidth = 80;
	private static final int matrixHeigth = 80;
	private static final int[][] matrix = new int[matrixWidth][matrixHeigth];
	private static final int[][] sums = new int[matrixWidth][matrixHeigth];
	
	public static int minimalPathSum() {
		// read the matrix
		try {
			URL input = Resources.getResource("_081_matrix.txt");
			int lineNumber = 0;
			for (String line : Resources.readLines(input, StandardCharsets.UTF_8)) {
				String[] numbers = line.split(",");
				for (int i = 0; i < numbers.length; i++) {
					matrix[lineNumber][i] = Integer.parseInt(numbers[i]);
				}
				lineNumber++;
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		return countSubtree(0, 0);
	}
	
	private static int countSubtree(int x, int y) {
		// bottom right corner, end recursion
		if ((x == matrixWidth -1) && (y == matrixHeigth -1)) {
			return matrix[x][y];
		}
		// return dynamically cached value, or create it
		int sum = sums[x][y];
		if (sum == 0) {
			if (x+1 >= matrixWidth) {
				// right edge
				sum = matrix[x][y] + countSubtree(x, y+1);
			} else if (y+1 >= matrixHeigth) {
				// bottom edge
				sum = matrix[x][y] + countSubtree(x+1, y);
			} else {
				sum = matrix[x][y]
						+ Math.min(countSubtree(x+1, y), countSubtree(x, y+1));
			}
			sums[x][y] = sum;
		}
		return sum;
	}

}