package cz.slanec.euler.problems50;

import java.math.BigInteger;

import cz.slanec.utils.Commons;

/**
 * If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.
 * <p>
 * Not all numbers produce palindromes so quickly. For example,<br />
 * 349 + 943 = 1292,<br />
 * 1292 + 2921 = 4213<br />
 * 4213 + 3124 = 7337
 * <p>
 * That is, 349 took three iterations to arrive at a palindrome.
 * <p>
 * Although no one has proved it yet, it is thought that some numbers, like 196,
 * never produce a palindrome. A number that never forms a palindrome through
 * the reverse and add process is called a Lychrel number. Due to the theoretical
 * nature of these numbers, and for the purpose of this problem, we shall assume
 * that a number is Lychrel until proven otherwise. In addition you are given that
 * for every number below ten-thousand, it will either<br />
 * (i) become a palindrome in less than fifty iterations, or,<br />
 * (ii) no one, with all the computing power that exists, has managed so far to map
 * it to a palindrome. In fact, 10677 is the first number to be shown to require
 * over fifty iterations before producing a palindrome: 4668731596684224866951378664
 * (53 iterations, 28-digits).
 * <p>
 * Surprisingly, there are palindromic numbers that are themselves Lychrel numbers;
 * the first example is 4994.
 * <p>
 * How many Lychrel numbers are there below ten-thousand?
 */
public class _055_LychrelNumbers {

	public static int howManyLychrelNumbersBelow10000() {
		int count = 0;
		for (int i = 0; i < 10000; i++) {
			if (!isLychrel(BigInteger.valueOf(i))) {
				count++;
			}
		}
		return count;
	}
	
	private static boolean isLychrel(BigInteger num) {
		for (int i = 0; i < 50; i++) {
			num = num.add(reverse(num));
			if (Commons.isPalindrome(num.toString())) {
				return true;
			}
		}
		return false;
	}
	
	private static BigInteger reverse(BigInteger num) {
		return new BigInteger(new StringBuilder(num.toString()).reverse().toString());
	}

}