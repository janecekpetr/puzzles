package cz.slanec.euler.problems50;

import java.util.List;
import java.util.NoSuchElementException;

import cz.slanec.utils.Digits;

/**
 * It can be seen that the number, 125874, and its double, 251748, contain
 * exactly the same digits, but in a different order.
 * <p>
 * Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x,
 * contain the same digits.
 */
public class _052_PermutedMultiples {
	private static final int MAX = 6;
	
	public static int smallestIntegerWhichMultipliedUsesSameDigits() {
		for (int x = 1; x < Integer.MAX_VALUE / MAX; x++) {
			if (doMultiplesContainSameDigits(x)) {
				return x;
			}
		}
		throw new NoSuchElementException();
	}

	private static boolean doMultiplesContainSameDigits(int x) {
		List<Byte> orig = Digits.decomposeNumber(x);
		for (int i = 2; i <= MAX; i++) {
			List<Byte> list = Digits.decomposeNumber(i*x);
			if (!list.containsAll(orig) || !orig.containsAll(list)) {
				return false;
			}
		}
		return true;
	}

}