package cz.slanec.euler.problems50;

import com.google.common.math.LongMath;

/**
 * There are exactly ten ways of selecting three from five, 12345:
 * 123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
 * <p>
 * In combinatorics, we use the notation, 5C3 = 10.
 * <p>
 * In general, nCr = n!/(r!(n−r)!), where r <= n, n! = n*(n−1)*...*3*2*1, and 0! = 1.
 * <p>
 * It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.
 * <p>
 * How many, not necessarily distinct, values of nCr, for 1 <= n <= 100, are
 * greater than one-million?
 */
public class _053_CombinatoricSelections {

	public static int howManyCombinationsAreGreaterThanAMillion() {
		int count = 0;
		for (int n = 1; n <= 100; n++) {
			for (int k = 0; k <= n; k++) {
				if (LongMath.binomial(n, k) > 1000000) {
					count++;
				}
			}
		}
		return count;
	}
	
}