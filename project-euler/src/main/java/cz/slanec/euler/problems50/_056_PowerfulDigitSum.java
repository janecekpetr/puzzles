package cz.slanec.euler.problems50;

import java.math.BigInteger;

import cz.slanec.utils.Digits;

/**
 * A googol (10^100) is a massive number: one followed by one-hundred zeros;
 * 100^100 is almost unimaginably large: one followed by two-hundred zeros.
 * Despite their size, the sum of the digits in each number is only 1.
 * <p>
 * Considering natural numbers of the form, a^b, where a, b < 100, what is
 * the maximum digital sum?
 */
public class _056_PowerfulDigitSum {

	public static int maximumPowerfulSum() {
		int max = 0;
		for (int a = 0; a < 100; a++) {
			for (int b = 0; b < 100; b++) {
				int sum = Digits.sumDigits(BigInteger.valueOf(a).pow(b).toString());
				max = Math.max(sum, max);
			}
		}
		return max;
	}

}