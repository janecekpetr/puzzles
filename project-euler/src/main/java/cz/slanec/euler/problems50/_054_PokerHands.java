package cz.slanec.euler.problems50;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.google.common.io.Resources;

/**
 * In the card game poker, a hand consists of five cards and are ranked, from
 * lowest to highest, in the following way:<br />
 * <ol>
 * <li>High Card: Highest value card.</li>
 * <li>One Pair: Two cards of the same value.</li>
 * <li>Two Pairs: Two different pairs.</li>
 * <li>Three of a Kind: Three cards of the same value.</li>
 * <li>Straight: All cards are consecutive values.</li>
 * <li>Flush: All cards of the same suit.</li>
 * <li>Full House: Three of a kind and a pair.</li>
 * <li>Four of a Kind: Four cards of the same value.</li>
 * <li>Straight Flush: All cards are consecutive values of same suit.</li>
 * <li>Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.</li>
 * </ol>
 * The cards are valued in the order:
 * 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
 * <p>
 * If two players have the same ranked hands then the rank made up of the
 * highest value wins; for example, a pair of eights beats a pair of fives. But
 * if two ranks tie, for example, both players have a pair of queens, then
 * highest cards in each hand are compared; if the highest cards tie then the
 * next highest cards are compared, and so on.
 * <p>
 * The file (http://projecteuler.net/project/poker.txt) contains one-thousand
 * random hands dealt to two players. Each line of the file contains ten cards
 * (separated by a single space): the first five are Player 1's cards and the
 * last five are Player 2's cards. You can assume that all hands are valid (no
 * invalid characters or repeated cards), each player's hand is in no specific
 * order, and in each hand there is a clear winner.
 * <p>
 * How many hands does Player 1 win?
 */
public class _054_PokerHands {

	/**
	 * Used to represent cards as well as whole hand ranks.<br />
	 * <p>
	 * Note: STRAIGHT_FLUSH omitted, it's STRAIGHT + FLUSH<br />
	 * ROYAL_FLUSH omitted, it's STRAIGHT + FLUSH + the highest card
	 */
	private enum Rank {
		TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE,
		ONE_PAIR, TWO_PAIRS, THREESOME,
		STRAIGHT, FLUSH, FULL_HOUSE, POKER;
		private static Rank valueOf(char c) {
			switch (c) {
				case '2': return TWO;
				case '3': return THREE;
				case '4': return FOUR;
				case '5': return FIVE;
				case '6': return SIX;
				case '7': return SEVEN;
				case '8': return EIGHT;
				case '9': return NINE;
				case 'T': return TEN;
				case 'J': return JACK;
				case 'Q': return QUEEN;
				case 'K': return KING;
				case 'A': return ACE;
			}
			return null;
		}
	}

	public static int howManyHandsWin() {
		int count = 0;

		try {
			URL input = Resources.getResource("_054_poker.txt");
			for (String line : Resources.readLines(input, StandardCharsets.UTF_8)) {
				String[] cards = line.split(" ");
				String[] hand1 = Arrays.copyOf(cards, 5);
				String[] hand2 = Arrays.copyOfRange(cards, 5, 10);
				if (handComparator.compare(hand1, hand2) > 0) {
					count++;
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return count;
	}

	private static final Comparator<String[]> handComparator = new Comparator<>() {
		@Override
		public int compare(String[] arr1, String[] arr2) {
			List<Rank> hand1 = evaluateHand(arr1);
			List<Rank> hand2 = evaluateHand(arr2);
			// reversed iterating, the hands are sorted in an ascending manner
			for (int i = 1; (hand1.size() - i >= 0) && (hand2.size() - i >= 0); i++) {
				int compared = hand1.get(hand1.size()-i).compareTo(hand2.get(hand2.size()-i));
				if (compared != 0) {
					return compared;
				}
			}
			return 0;
		}

		private List<Rank> evaluateHand(String[] hand) {
			// a sorted list of all cards represented by Rank
			List<Rank> cards = new ArrayList<>();
			for (String card : hand) {
				cards.add(Rank.valueOf(card.charAt(0)));
			}
			Collections.sort(cards);

			// whether all cards are of the same suit
			boolean allCardsSameSuit = true;
			char c = hand[0].charAt(1);
			for (int i = 1; i < hand.length; i++) {
				if (hand[i].charAt(1) != c) {
					allCardsSameSuit = false;
					break;
				}
			}

			// iterate over a copy of cards
			Iterator<Rank> iter = new ArrayList<>(cards).iterator();
			Rank lastCard = iter.next();
			int sameCards = 1;	// for pairs and threesomes
			int consecutiveCards = 1;	// for straights
			while (iter.hasNext()) {
				Rank card = iter.next();
				if (card == lastCard) {
					sameCards++;
					// not the last card
					if (iter.hasNext()) {
						continue;
					}
				}

				// (card == lastCard+1) or (card == A and lastCard == 5 ... special case for A2345)
				if ((card == Rank.values()[lastCard.ordinal() + 1]) || (card == Rank.ACE && lastCard == Rank.FIVE)) {
					consecutiveCards++;
				} else {
					consecutiveCards = 1;
				}

				switch (sameCards) {
					case 1:
						break;
					case 2:
						cards.add(lastCard);	// saves the value of the pair
						// for two pairs
						if (cards.contains(Rank.ONE_PAIR)) {
							cards.add(Rank.ONE_PAIR);
							cards.add(Rank.TWO_PAIRS);
							break;
						}
						cards.add(Rank.ONE_PAIR);
						if (cards.contains(Rank.THREESOME)) {
							// swaps the threesome the the higher position for comparing
							Collections.swap(cards, cards.size()-1, cards.size()-3);
							Collections.swap(cards, cards.size()-2, cards.size()-4);
							cards.add(Rank.FULL_HOUSE);
							break;
						}
						break;
					case 3:
						cards.add(lastCard);	// saves the value of the threesome
						cards.add(Rank.THREESOME);
						if (cards.contains(Rank.ONE_PAIR)) {
							cards.add(Rank.FULL_HOUSE);
						}
						break;
					case 4:
						cards.add(lastCard);	// saves the value of the poker
						cards.add(Rank.POKER);
						break;
					default:
						throw new IllegalArgumentException("sameCards: " + sameCards);
				}
				sameCards = 1;
				lastCard = card;
			}

			if (consecutiveCards == 5) {
				cards.add(Rank.STRAIGHT);
			}
			if (allCardsSameSuit) {
				cards.add(Rank.FLUSH);
			}

			return cards;
		}
	};

}