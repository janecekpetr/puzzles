package cz.slanec.euler.problems50;

import java.util.ArrayDeque;
import java.util.Deque;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;

/**
 * The prime 41, can be written as the sum of six consecutive primes:<br />
 * 41 = 2 + 3 + 5 + 7 + 11 + 13
 * <p>
 * This is the longest sum of consecutive primes that adds to a prime below
 * one-hundred.
 * <p>
 * The longest sum of consecutive primes below one-thousand that adds to a prime,
 * contains 21 terms, and is equal to 953.
 * <p>
 * Which prime, below one-million, can be written as the sum of the most
 * consecutive primes?
 */
public class _050_ConsecutivePrimeSum {
	private static final int MAX = 1_000_000;
	
	public static int primeWhichIsWrittenAsASumOfTheMostConsecutivePrimes() {
		Sieve sieve = new Eratosthenes_Array((int)(1.1*MAX));
		
		Deque<Integer> seq = new ArrayDeque<>();
		int prime = sieve.nextPrime(0);
		int sum = 0;
		// makes the max length sequence possible (actually, just a bit long)
		while (sum < MAX) {
			seq.add(prime);
			sum += prime;
			prime = sieve.nextPrime(prime);
		}
		
		while (!sieve.isPrime(sum)) {
			// make the sequence shorter
			sum -= seq.removeLast();
			
			int sum2 = sum;
			Deque<Integer> seq2 = new ArrayDeque<>(seq);
			while (sum2 < MAX) {
				// offset the existing sequence if possible
				sum2 -= seq2.removeFirst();
				int newLast = sieve.nextPrime(seq2.getLast());
				sum2 += newLast;
				seq2.add(newLast);
				if (sieve.isPrime(sum2)) {
					seq = seq2;
					sum = sum2;
					break;
				}
			}
		}
		
		return sum;
	}

}