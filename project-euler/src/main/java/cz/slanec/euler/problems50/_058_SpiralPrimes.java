package cz.slanec.euler.problems50;

import cz.slanec.factorization.Factorization;

/**
 * Starting with 1 and spiralling anticlockwise in the following way, a square
 * spiral with side length 7 is formed.<pre>
 * 37 36 35 34 33 32 31
 * 38 17 16 15 14 13 30
 * 39 18  5  4  3 12 29
 * 40 19  6  1  2 11 28
 * 41 20  7  8  9 10 27
 * 42 21 22 23 24 25 26
 * 43 44 45 46 47 48 49</pre>
 * <p>
 * It is interesting to note that the odd squares lie along the bottom right
 * diagonal, but what is more interesting is that 8 out of the 13 numbers lying
 * along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.
 * <p>
 * If one complete new layer is wrapped around the spiral above, a square spiral
 * with side length 9 will be formed. If this process is continued, what is
 * the side length of the square spiral for which the ratio of primes along both
 * diagonals first falls below 10%?
 */
public class _058_SpiralPrimes {
	
	public static int howLongWillSpiralSideBeWhenTheRatioOnDiagonalsFallsBelow10Percent() {
		int num = 1;
		int primes = 0;
		int numbers = 1;
		
		int growth = 2;
		// the right bottom corner always has the value of height*width
		do {
			// one full counter-clockwise turn starting right-top
			for (int i = 0; i < 4; i++) {
				num += growth;
				if (Factorization.isPrime(num)) {
					primes++;
				}
			}
			numbers += 4;
			growth += 2;
		} while ((float)primes/numbers >= 0.1);
		
		return (growth - 1);
	}

}