package cz.slanec.euler.problems50;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;
import cz.slanec.utils.Digits;

import com.google.common.collect.Collections2;

/**
 * By replacing the 1st digit of *3, it turns out that six of the nine possible
 * values: 13, 23, 43, 53, 73, and 83, are all prime.
 * <p>
 * By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit
 * number is the first example having seven primes among the ten generated numbers,
 * yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993.
 * <p>
 * Consequently 56003, being the first member of this family, is the smallest prime
 * with this property.
 * <p>
 * Find the smallest prime which, by replacing part of the number (not necessarily
 * adjacent digits) with the same digit, is part of an eight prime value family.
 */
public class _051_PrimeDigitReplacements {
	private static final byte JOKER = -1;
	
	// errs for seven prime value family, but who cares =)
	public static int smalestPrimeWhichIsPartOfAnEightPrimeFamilyWhenSubnumberReplaced() {
		Sieve sieve = new Eratosthenes_Array();
		int maxCount = 0;
		int maxPrime = 0;
		
		for (int numOfJokers = 1; numOfJokers <= 3; numOfJokers++) {
			for (int i = 1; i < 1000; i++) {
				// the number + jokers
				List<Byte> digitsWithJokers = addJokers(Digits.decomposeNumber(i), numOfJokers);
				// all permutations of the number + jokers
				Collection<List<Byte>> perms = Collections2.permutations(digitsWithJokers);
				for (List<Byte> digits : perms) {
					List<Integer> numbers = buildNums(digits);
					int count = 0;
					int firstPrime = 0;
					for (int possiblePrime : numbers) {
						if (sieve.isPrime(possiblePrime)) {
							if (count == 0) {
								firstPrime = possiblePrime;
							}
							count++;
						}
					}
					if (count > maxCount) {
						maxCount = count;
						maxPrime = firstPrime;
					}
				}
			}
		}
		
		return maxPrime;
	}
	
	private static List<Integer> buildNums(List<Byte> digits) {
		List<Integer> result = new ArrayList<>();
		jokerLoop:
		for (int jokerValue = 0; jokerValue < 10; jokerValue++) {
			int num = 0;
			for (int digit : digits) {
				// starts with a 0 or a joker that would be replaced with a 0
				if (((digit == 0) && (num == 0)) || ((digit == JOKER) && (jokerValue == 0))) {
					continue jokerLoop;
				}
				if (digit == JOKER) {
					num = num*10 + jokerValue;
				} else {
					num = num*10 + digit;
				}
			}
			result.add(num);
		}
		return result;
	}
	
	private static List<Byte> addJokers(List<Byte> digits, int numOfJokers) {
		while (numOfJokers --> 0) {
			digits.add(JOKER);
		}
		return digits;
	}
	
}