package cz.slanec.euler.problems50;

import static cz.slanec.utils.Commons.xor;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import cz.slanec.recognizetext.Language;
import cz.slanec.utils.Strings;

import com.google.common.io.Resources;

/**
 * Each character on a computer is assigned a unique code and the preferred standard
 * is ASCII (American Standard Code for Information Interchange). For example,
 * uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.
 * <p>
 * A modern encryption method is to take a text file, convert the bytes to ASCII,
 * then XOR each byte with a given value, taken from a secret key. The advantage
 * with the XOR function is that using the same encryption key on the cipher text,
 * restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.
 * <p>
 * For unbreakable encryption, the key is the same length as the plain text message,
 * and the key is made up of random bytes. The user would keep the encrypted message
 * and the encryption key in different locations, and without both "halves", it is
 * impossible to decrypt the message.
 * <p>
 * Unfortunately, this method is impractical for most users, so the modified method
 * is to use a password as a key. If the password is shorter than the message, which
 * is likely, the key is repeated cyclically throughout the message. The balance for
 * this method is using a sufficiently long password key for security, but short
 * enough to be memorable.
 * <p>
 * Your task has been made easy, as the encryption key consists of three lower case
 * characters. Using http://projecteuler.net/project/cipher1.txt, a file containing
 * the encrypted ASCII codes, and the knowledge that the plain text must contain
 * common English words, decrypt the message and find the sum of the ASCII values
 * in the original text.
 */
public class _059_XORDecryption {
	
	public static int sumThreeCharXorKey() {
		// read input
		byte[] encrypted = null;
		try {
			URL input = Resources.getResource("_059_cipher.txt");
			String[] line = Resources.toString(input, StandardCharsets.UTF_8).split(",");
			encrypted = new byte[line.length];
			for (int i = 0; i < encrypted.length; i++) {
				encrypted[i] = Byte.parseByte(line[i]);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		List<String> candidates = new ArrayList<>();
		// for every key
		for (byte a = 'a'; a <= 'z'; a++) {
			for (byte b = 'a'; b <= 'z'; b++) {
				for (byte c = 'a'; c <= 'z'; c++) {
					byte[] key = new byte[] {a, b, c};
					// try to build a decrypted String
					byte[] decrypted = xor(encrypted, key);
					String candidate = new String(decrypted);
					if (Strings.isPrintable(candidate)) {
						candidates.add(candidate);
					}
				}
			}
		}
		
		// a candidate found, perform a heuristic check
		String winner = Language.ENGLISH.recognizeFrom(candidates);
		return sumASCIIValues(winner);
	}
	
	private static int sumASCIIValues(String str) {
		int sum = 0;
		for (int i = 0; i < str.length(); i++) {
			sum += str.charAt(i);
		}
		return sum;
	}
}