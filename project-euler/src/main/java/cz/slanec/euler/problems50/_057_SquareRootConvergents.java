package cz.slanec.euler.problems50;

import java.math.BigInteger;

/**
 * It is possible to show that the square root of two can be expressed
 * as an infinite continued fraction.<br />
 * sqrt(2) = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...
 * <p>
 * By expanding this for the first four iterations, we get:<br />
 * 1 + 1/2 = 3/2 = 1.5<br />
 * 1 + 1/(2 + 1/2) = 7/5 = 1.4<br />
 * 1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...<br />
 * 1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...
 * <p>
 * The next three expansions are 99/70, 239/169, and 577/408, but the eighth
 * expansion, 1393/985, is the first example where the number of digits
 * in the numerator exceeds the number of digits in the denominator.
 * <p>
 * In the first one-thousand expansions, how many fractions contain a numerator
 * with more digits than denominator?
 */
public class _057_SquareRootConvergents {
	private static final BigInteger TWO = BigInteger.valueOf(2);
	
	public static int howManyFractionsContainANumeratorWithMoreDigitsThanDenominator() {
		// the new numerator is 2*numerator + lastNumerator
		BigInteger numerator = BigInteger.valueOf(7);
		BigInteger denominator = BigInteger.valueOf(5);
		BigInteger lastNumerator = BigInteger.valueOf(3);
		BigInteger lastDenominator = BigInteger.valueOf(2);

		int count = 0;
		for (int i = 2; i < 1000; i++) {
			BigInteger origNumerator = numerator;
			BigInteger origDenominator = denominator;
			numerator = numerator.multiply(TWO).add(lastNumerator);
			denominator = denominator.multiply(TWO).add(lastDenominator);
			lastNumerator = origNumerator;
			lastDenominator = origDenominator;
			if (numerator.toString().length() > denominator.toString().length()) {
				count++;
			}
		}
		
		return count;
	}

}