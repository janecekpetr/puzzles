package cz.slanec.euler.problems40;

import cz.slanec.utils.Digits;

/**
 * An irrational decimal fraction is created by concatenating the positive integers:
 * 0.123456789101112131415161718192021...
 * <p>
 * It can be seen that the 12th digit of the fractional part is 1.
 * <p>
 * If d(n) represents the nth digit of the fractional part, find the value
 * of the following expression.
 * <p>
 * d(1) × d(10) × d(100) × d(1000) × d(10000) × d(100000) × d(1000000)
 */
public class _040_ChampernownesConstant {
	private static final int MAX = 1_000_000;
	
	public static int champernownesConstant() {
		// one char to index more naturally into the String
		StringBuilder s = new StringBuilder(" ");
		int num = 1;
		while (s.length() <= MAX) {
			s.append(num++);
		}
		
		int result = 1;
		for (int i = 1; i <= MAX; i *= 10) {
			result *= Digits.digit(s, i);
		}
		
		return result;
	}

}