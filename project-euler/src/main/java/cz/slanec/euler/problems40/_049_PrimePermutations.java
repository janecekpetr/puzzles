package cz.slanec.euler.problems40;

import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;
import cz.slanec.utils.Commons;
import cz.slanec.utils.Digits;

import com.google.common.collect.Collections2;

/**
 * The arithmetic sequence, 1487, 4817, 8147, in which each of the terms
 * increases by 3330, is unusual in two ways:<br />
 * (i) each of the three terms are prime, and,<br />
 * (ii) each of the 4-digit numbers are permutations of one another.
 * <p>
 * There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes,
 * exhibiting this property, but there is one other 4-digit increasing sequence.
 * <p>
 * What 12-digit number do you form by concatenating the three terms in this sequence?
 */
public class _049_PrimePermutations {

	public static long concatenateNumbersThatAreAllPrimesAndPermutations () {
		Sieve sieve = new Eratosthenes_Array(10000);
		
		for (int prime = sieve.nextPrime(1000); prime + 6660 < 10000; prime = sieve.nextPrime(prime)) {
			if (prime == 1487 || prime == 4817 || prime == 8147) {
				continue;
			}
			
			int second = prime + 3330;
			int third = second + 3330;
			if (sieve.isPrime(second) && sieve.isPrime(third)) {
				Collection<List<Byte>> perms = Collections2.permutations(Digits.decomposeNumber(prime));
				if (perms.contains(Digits.decomposeNumber(second)) && perms.contains(Digits.decomposeNumber(third))) {
					return Commons.concatenate(prime, second, third);
				}
			}
		}
		
		throw new NoSuchElementException();
	}
	
}