package cz.slanec.euler.problems40;

import static cz.slanec.utils.Commons.isDivisable;

import java.util.Collection;
import java.util.List;

import cz.slanec.euler.util.PandigitalUtils;
import cz.slanec.utils.Digits;

/**
 * The number, 1406357289, is a 0 to 9 pandigital number because it is
 * made up of each of the digits 0 to 9 in some order, but it also has
 * a rather interesting sub-string divisibility property.
 * <p>
 * Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way,
 * we note the following:<br />
 * d2d3d4=406 is divisible by 2<br />
 * d3d4d5=063 is divisible by 3<br />
 * d4d5d6=635 is divisible by 5<br />
 * d5d6d7=357 is divisible by 7<br />
 * d6d7d8=572 is divisible by 11<br />
 * d7d8d9=728 is divisible by 13<br />
 * d8d9d10=289 is divisible by 17
 * <p>
 * Find the sum of all 0 to 9 pandigital numbers with this property.
 */
public class _043_SubstringDivisibility {

	public static long sumPandigitalNumbersWithNicelyDivisibleSubstrings() {
		long sum = 0;
		
		// all permutations of 0 to 9 pandigital numbers
		// (those starting with a zero are included too)
		Collection<List<Byte>> digitPermutations = PandigitalUtils.getPandigitals(0, 9);
		for (List<Byte> digits : digitPermutations) {
			long num = Digits.composeNumber(digits);
			// limit to over a billion to avoid those numbers starting with a zero
			if (num > 1_000_000_000 && isSubstringDivisable(num)) {
				sum += num;
			}
		}
		
		return sum;
	}
	
	private static boolean isSubstringDivisable(long num) {
		String str = String.valueOf(num);

		int sub2 = 100*Digits.digit(str, 1) + 10*Digits.digit(str, 2) + Digits.digit(str, 3);
		int sub3 = rotate(sub2, str, 4);
		int sub4 = rotate(sub3, str, 5);
		int sub5 = rotate(sub4, str, 6);
		int sub6 = rotate(sub5, str, 7);
		int sub7 = rotate(sub6, str, 8);
		int sub8 = rotate(sub7, str, 9);
		return isDivisable(sub2, 2)
				&& isDivisable(sub3, 3)
				&& isDivisable(sub4, 5)
				&& isDivisable(sub5, 7)
				&& isDivisable(sub6, 11)
				&& isDivisable(sub7, 13)
				&& isDivisable(sub8, 17);
	}
	
	// gets the next number
	private static int rotate(int num, String str, int index) {
		return (num % 100) * 10 + Digits.digit(str, index);
	}
	
}