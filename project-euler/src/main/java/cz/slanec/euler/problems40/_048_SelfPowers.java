package cz.slanec.euler.problems40;

import java.math.BigInteger;

/**
 * The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.
 * <p>
 * Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
 */
public class _048_SelfPowers {
	private static final BigInteger TEN_DIGITS = BigInteger.valueOf(10_000_000_000L);
	
	public static long lastTenDigitsOfSumOfSelfPowersUpToAThousand() {
		BigInteger num = BigInteger.ONE;
		for (int i = 2; i <= 1000; i++) {
			BigInteger b = BigInteger.valueOf(i);
			num = num.add(b.modPow(b, TEN_DIGITS));
		}
		return num.mod(TEN_DIGITS).longValue();
	}

}