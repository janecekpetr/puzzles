package cz.slanec.euler.problems40;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import cz.slanec.utils.Commons;

import com.google.common.io.Resources;

/**
 * The nth term of the sequence of triangle numbers is given by, t(n) = n(n+1)/2;
 * so the first ten triangle numbers are: 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
 * <p>
 * By converting each letter in a word to a number corresponding to its
 * alphabetical position and adding these values we form a word value. For example,
 * the word value for SKY is 19 + 11 + 25 = 55 = t(10). If the word value is
 * a triangle number then we shall call the word a triangle word.
 * <p>
 * Using words.txt (http://projecteuler.net/project/words.txt), a 16K text file
 * containing nearly two-thousand common English words, how many are triangle words?
 */
public class _042_CodedTriangleNumbers {

	public static int howManyTriangleWords() {
		// all triangle numbers less than t(50)
		Set<Integer> triangles = new HashSet<>();
		int triangle = 0;
		for (int growth = 1; growth < 50; growth++) {
			triangle += growth;
			triangles.add(triangle);
		}
		
		// all the words from the file
		String[] words = null;
		try {
			URL input = Resources.getResource("_042_words.txt");
			words = Resources.toString(input, StandardCharsets.UTF_8).replace("\"", "").split(",");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		int count = 0;
		for (String word : words) {
			if (triangles.contains(Commons.sumWord(word))) {
				count++;
			}
		}
		return count;
	}
	
}