package cz.slanec.euler.problems40;

import java.util.Collection;
import java.util.List;

import cz.slanec.euler.util.PandigitalUtils;
import cz.slanec.factorization.Factorization;
import cz.slanec.utils.Digits;

/**
 * We shall say that an n-digit number is pandigital if it makes use of all
 * the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital
 * and is also prime.
 * <p>
 * What is the largest n-digit pandigital prime that exists?
 */
public class _041_PandigitalPrime {

	public static int largestPandigitalPrime() {
		int max = 0;
		
		// begin with 9 digits, go lower then
		for (int numberOfDigits = 9; numberOfDigits > 0; numberOfDigits--) {
			// all pandigital numbers of this many digits
			Collection<List<Byte>> digitPermutations = PandigitalUtils.getPandigitals(1, numberOfDigits);
			for (List<Byte> digits : digitPermutations) {
				int num = (int)Digits.composeNumber(digits);
				if (Factorization.isPrime(num)) {
					max = Math.max(num, max);
				}
			}
			if (max > 0) {
				// max found, it won't get better with fewer digits
				break;
			}
		}
		
		return max;
	}
	
}