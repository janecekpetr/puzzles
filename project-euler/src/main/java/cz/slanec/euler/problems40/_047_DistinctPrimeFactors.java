package cz.slanec.euler.problems40;

import java.util.NoSuchElementException;

import cz.slanec.factorization.Factorization;

/**
 * The first two consecutive numbers to have two distinct prime factors are:<br />
 * 14 = 2 * 7<br />
 * 15 = 3 * 5
 * <p>
 * The first three consecutive numbers to have three distinct prime factors are:<br />
 * 644 = 2^2 * 7 * 23<br />
 * 645 = 3 * 5 * 43<br />
 * 646 = 2 * 17 * 19.
 * <p>
 * Find the first four consecutive integers to have four distinct prime factors.
 * What is the first of these numbers?
 */
public class _047_DistinctPrimeFactors {
	private static final int MAX = 1000000;
	private static final int HOW_MANY_CONSECUTIVE = 4;
	
	public static int firstFourIntegersToHaveFourDistincsPrimeFactors() {
		int consecutive = 0;
		for (int i = 1; i < MAX; i++) {
			if (Factorization.factorize(i).size() == HOW_MANY_CONSECUTIVE) {
				consecutive++;
				if (consecutive == HOW_MANY_CONSECUTIVE) {
					return (i - HOW_MANY_CONSECUTIVE + 1);
				}
			} else {
				consecutive = 0;
			}
		}
		throw new NoSuchElementException();
	}

}