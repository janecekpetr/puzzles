package cz.slanec.euler.problems40;

import java.util.HashSet;
import java.util.Set;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;

/**
 * It was proposed by Christian Goldbach that every odd composite number
 * can be written as the sum of a prime and twice a square.
 * <p>
 *  9 =  7 + 2×1^2<br />
 * 15 =  7 + 2×2^2<br />
 * 21 =  3 + 2×3^2<br />
 * 25 =  7 + 2×3^2<br />
 * 27 = 19 + 2×2^2<br />
 * 33 = 31 + 2×1^2
 * <p>
 * It turns out that the conjecture was false.
 * <p>
 * What is the smallest odd composite that cannot be written as the sum of
 * a prime and twice a square?
 */
public class _046_GoldbachsOtherConjecture {
	private static final int MAX = 100_000;
	
	public static int smallestCompositeThatIsNotASumOfPrimeAndTwiceASquare() {
		Set<Integer> twiceSquares = new HashSet<>();
		for (int i = 1; i*i <= MAX; i++) {
			twiceSquares.add(2*i*i);
		}
		
		Sieve sieve = new Eratosthenes_Array((int)(1.1*MAX));
		int composite = 9;
		
		outerLoop:
		while (true) {
			composite += 2;
			if (sieve.isPrime(composite)) {
				continue;
			}
			for (int prime = 3; prime < MAX; prime = sieve.nextPrime(prime)) {
				if (twiceSquares.contains((composite - prime))) {
					continue outerLoop;
				}
			}
			return composite;
		}
	}

}