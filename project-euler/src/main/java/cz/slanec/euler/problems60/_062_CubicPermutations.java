package cz.slanec.euler.problems60;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.slanec.utils.Digits;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

/**
 * The cube, 41063625 (345^3), can be permuted to produce two other cubes:
 * 56623104 (384^3) and 66430125 (405^3). In fact, 41063625 is the smallest cube
 * which has exactly three permutations of its digits which are also cube.
 * <p>
 * Find the smallest cube for which exactly five permutations of its digits are cube.
 */
public class _062_CubicPermutations {
	private static final int THRESHOLD = 5;
	
	public static long smallestCubeWhichHasFiveCubePermutations() {
		Multiset<List<Byte>> cubes = HashMultiset.create();
		Map<List<Byte>, Long> minims = new HashMap<>();
		long base = 0;
		while (true) {
			base++;
			long cube = base*base*base;
			List<Byte> digits = Digits.decomposeNumber(cube);
			Collections.sort(digits);
			cubes.add(digits);
			int count = cubes.count(digits);
			if (count == 1) {
				minims.put(digits, cube);
			}
			if (count == THRESHOLD) {
				return minims.get(digits);
			}
		}
	}
	
}