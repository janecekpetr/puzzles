package cz.slanec.euler.problems60;

import cz.slanec.utils.Commons;

/**
 * Consider quadratic Diophantine equations of the form: x^2 – Dy^2 = 1
 * <p>
 * For example, when D=13, the minimal solution in x is 649^2 – 13×180^2 = 1.
 * <p>
 * It can be assumed that there are no solutions in positive integers when D
 * is square.
 * <p>
 * By finding minimal solutions in x for D = {2, 3, 5, 6, 7}, we obtain
 * the following: <br />
 * 3^2 – 2×2^2 = 1<br />
 * 2^2 – 3×1^2 = 1<br />
 * 9^2 – 5×4^2 = 1<br />
 * 5^2 – 6×2^2 = 1<br />
 * 8^2 – 7×3^2 = 1<br />
 * <p>
 * Hence, by considering minimal solutions in x for D ≤ 7, the largest x
 * is obtained when D=5.
 * <p>
 * Find the value of D ≤ 1000 in minimal solutions of x for which the largest
 * value of x is obtained.
 */
public class _066_DiophantineEquation {
	private static final int MAX_D = 1000;
	
	// TODO after 64 and 65
	public static void main(String[] args) {
		long maxX = 0;
		int maxD = 0;
		
		for (int D = 2; D <= MAX_D; D++) {
			System.out.println(D);
			if (Commons.isInteger(Math.sqrt(D))) {
				continue;
			}
			long y = 1;
			double x = Math.sqrt(D*y*y + 1);
			while (!Commons.isInteger(x)) {
				y++;
				x = Math.sqrt(D*y*y + 1);
			}
//			System.out.println((long)x+"^2 -" +D+"*"+y+"^2" + " = 1");
			if (x > maxX) {
				maxX = (long)x;
				maxD = D;
			}
		}
		
		System.out.println("max D: " + maxD);
		System.out.println("max X: " + maxX);
	}

}