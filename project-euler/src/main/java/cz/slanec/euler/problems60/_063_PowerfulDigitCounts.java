package cz.slanec.euler.problems60;

import java.math.BigInteger;

/**
 * The 5-digit number, 16807 = 7^5, is also a fifth power. Similarly, the
 * 9-digit number, 134217728 = 8^9, is a ninth power.
 * <p>
 * How many n-digit positive integers exist which are also an nth power?
 */
public class _063_PowerfulDigitCounts {

	public static int howManyNDigitIntegersAreAlsoNthPower() {
		int count = 0;
		// base >= 10 doesn't make sense, the length would always be > exp
		for (int base = 1; base < 10; base++) {
			BigInteger bigBase = BigInteger.valueOf(base);
			int exp = 1;
			while (true) {
				BigInteger pow = bigBase.pow(exp);
				int length = pow.toString().length();
				// (length <= exp) since we're multiplying by numbers < 10
				if (length == exp) {
					count++;
				} else if (length < exp) {
					// it would never catch up again
					break;
				}
				exp++;
			}
		}
		return count;
	}
	
}