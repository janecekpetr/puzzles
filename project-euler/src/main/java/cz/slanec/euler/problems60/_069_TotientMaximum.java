package cz.slanec.euler.problems60;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;

/**
 * Euler's Totient function, phi(n) [sometimes called the phi function], is used
 * to determine the number of numbers less than n which are relatively prime to n.
 * <p>
 * For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively
 * prime to nine, phi(9)=6.
 * <p>
 * <table>
 * <tr><td>n</td><td>Relatively prime</td><td>phi(n)</td><td>n/phi(n)</td></tr>
 * <tr><td>2</td><td>1</td><td>1</td><td>2</td></tr>
 * <tr><td>3</td><td>1,2</td><td>2</td><td>1.5</td></tr>
 * <tr><td>4</td><td>1,3</td><td>2</td><td>2</td></tr>
 * <tr><td>5</td><td>1,2,3,4</td><td>4</td><td>1.25</td></tr>
 * <tr><td>6</td><td>1,5</td><td>2</td><td>3</td></tr>
 * <tr><td>7</td><td>1,2,3,4,5,6</td><td>6</td><td>1.1666...</td></tr>
 * <tr><td>8</td><td>1,3,5,7</td><td>4</td><td>2</td></tr>
 * <tr><td>9</td><td>1,2,4,5,7,8</td><td>6</td><td>1.5</td></tr>
 * <tr><td>10</td><td>1,3,7,9</td><td>4</td><td>2.5</td></tr>
 * </table>
 * <p>
 * It can be seen that n=6 produces a maximum n/phi(n) for n ≤ 10.
 * <p>
 * Find the value of n ≤ 1,000,000 for which n/phi(n) is a maximum.
 */
public class _069_TotientMaximum {

	// 2*3*4*5... etc.
	public static int maxRatioOfNAndTotientNBelowAMillion() {
		Sieve sieve = new Eratosthenes_Array(100);
		
		int prime = 2;
		int num = prime;
		while (num <= 1000000) {
			prime = sieve.nextPrime(prime);
			num *= prime;
		}
		num /= prime;
		
		return num;
	}
	
}