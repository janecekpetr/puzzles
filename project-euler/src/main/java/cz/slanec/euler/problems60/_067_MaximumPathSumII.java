package cz.slanec.euler.problems60;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.google.common.io.Resources;

/**
 * By starting at the top of the triangle below and moving to adjacent numbers
 * on the row below, the maximum total from top to bottom is 23.<pre>
 * 3
 * 7 4
 * 2 4 6
 * 8 5 9 3</pre>
 * That is, 3 + 7 + 4 + 9 = 23.
 * <p>
 * Find the maximum total from top to bottom in
 * http://projecteuler.net/project/triangle.txt, a 15K text file containing
 * a triangle with one-hundred rows.
 * <p>
 * NOTE: This is a much more difficult version of Problem 18. It is not possible
 * to try every route to solve this problem, as there are 299 altogether! If you
 * could check one trillion (1012) routes every second it would take over twenty
 * billion years to check them all. There is an efficient algorithm to solve it. ;o)
 */
public class _067_MaximumPathSumII {
	private static int[][] triangle;
	private static int[][] sums;
	
	public static int findMaximumSumPathInATriangle() {
		// read the triangle
		try {
			URL input = Resources.getResource("_067_triangle.txt");
			List<int[]> rows = new ArrayList<>();
			for (String line : Resources.readLines(input, StandardCharsets.UTF_8)) {
				String[] lineParts = line.split(" ");
				int[] numbers = new int[lineParts.length];
				for (int i = 0; i < lineParts.length; i++) {
					numbers[i] = Integer.parseInt(lineParts[i]);
				}
				rows.add(numbers);
			}
			triangle = new int[rows.size()][];
			triangle = rows.toArray(triangle);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		// allocate a new empty triangle for dynamic sums caching
		sums = new int[triangle.length][];
		for (int line = 0; line < triangle.length; line++) {
			sums[line] = new int[triangle[line].length];
		}
		
		return countSubtree(0, 0);
	}
	
	private static int countSubtree(int level, int pos) {
		// lowest level, end recursion
		if (level == triangle.length - 1) {
			return triangle[level][pos];
		}
		// return dynamically cached value, or create it
		int sum = sums[level][pos];
		if (sum == 0) {
			sum = triangle[level][pos]
					+ Math.max(countSubtree(level+1, pos), countSubtree(level+1, pos+1));
			sums[level][pos] = sum;
		}
		return sum;
	}
	
}