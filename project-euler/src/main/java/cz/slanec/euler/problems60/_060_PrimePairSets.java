package cz.slanec.euler.problems60;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;
import cz.slanec.utils.Commons;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * The primes 3, 7, 109, and 673, are quite remarkable. By taking any two primes
 * and concatenating them in any order the result will always be prime. For example,
 * taking 7 and 109, both 7109 and 1097 are prime. The sum of these four primes,
 * 792, represents the lowest sum for a set of four primes with this property.
 * <p>
 * Find the lowest sum for a set of five primes for which any two primes concatenate
 * to produce another prime.
 */
public class _060_PrimePairSets {
	private static final int MAX = 10000;
	
	public static int lowestSumOfFivePrimesThatConcatenatedAlsoProduceAPrime() {
		// all the considered sets, contains all primes initially
		List<List<Integer>> progressions = new ArrayList<>();
		final Sieve sieve = new Eratosthenes_Array(MAX*MAX);
		for (int prime = sieve.nextPrime(0); prime < MAX; prime = sieve.nextPrime(prime)) {
			progressions.add(Collections.singletonList(prime));
		}
		
		// while the sets contain less than 5 primes
		while (progressions.get(0).size() < 5) {
			List<List<Integer>> newProgressions = new ArrayList<>();
			for (List<Integer> progression : progressions) {
				progressionLoop:
				for (int prime = sieve.nextPrime(Collections.max(progression)); prime < MAX; prime = sieve.nextPrime(prime)) {
					for (int knownPrime : progression) {
						int probablePrime1 = (int)Commons.concatenate(prime, knownPrime);
						int probablePrime2 = (int)Commons.concatenate(knownPrime, prime);
						if (!sieve.isPrime(probablePrime1) || !sieve.isPrime(probablePrime2)) {
							continue progressionLoop;
						}
					}
					List<Integer> newProgression = Lists.newArrayList(progression);
					newProgression.add(prime);
					newProgressions.add(newProgression);
				}
			}
			progressions = newProgressions;
		}
		
		List<Integer> resultProgression = Iterables.getOnlyElement(progressions);
		return Commons.sum(resultProgression);
	}
	
}