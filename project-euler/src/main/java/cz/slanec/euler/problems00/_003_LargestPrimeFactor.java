package cz.slanec.euler.problems00;

import cz.slanec.factorization.Factorization;

import com.google.common.collect.Iterables;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * <p>
 * What is the largest prime factor of the number 600851475143?
 */
public class _003_LargestPrimeFactor {

	public static long getLastPrimeFactor(long number) {
		return Iterables.getLast(Factorization.factorize(number)).factor;
	}
	
}