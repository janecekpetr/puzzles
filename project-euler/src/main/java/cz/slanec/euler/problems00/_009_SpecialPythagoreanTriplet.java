package cz.slanec.euler.problems00;

import java.util.NoSuchElementException;

/**
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which
 * a^2 + b^2 = c^2
 * <p>
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 * <p>
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product a*b*c.
 */
public class _009_SpecialPythagoreanTriplet {
	
	public static int getSpecialPythagoreanTriplet() {
		for (int c = 1000; c > 0; c--) {
			for (int a = 1; a + c < 1000; a++) {
				int b = 1000 - (a+c);
				if (a*a + b*b == c*c) {
					return a*b*c;
				}
			}
		}
		throw new NoSuchElementException();
	}

}