package cz.slanec.euler.problems00;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, 13, we can see
 * that the 6th prime is 13.
 * <p>
 * What is the 10 001<sup>st</sup> prime number?
 */
public class _007_10001stPrime {

	public static int get10001stPrime() {
		Sieve sieve = new Eratosthenes_Array();
		int prime = 1;
		for (int i = 0; i < 10001; i++) {
			prime = sieve.nextPrime(prime);
		}
		return prime;
	}
	
}