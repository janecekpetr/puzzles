package cz.slanec.euler.problems00;

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we
 * get 3, 5, 6 and 9. The sum of these multiples is 23.
 * <p>
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
public class _001_MultiplesOf3And5 {

	public static int sum3sAnd5s(int threshold) {
		int sum = 0;
		for (int i = 0; i < threshold; i += 3) {
			sum += i;
		}
		for (int i = 0; i < threshold; i += 5) {
			if (i % 3 != 0) {
				sum += i;
			}
		}
		return sum;
	}
	
}