package cz.slanec.euler.problems00;

import cz.slanec.utils.Commons;

/**
 * A palindromic number reads the same both ways. The largest palindrome
 * made from the product of two 2-digit numbers is 9009 = 91*99.
 * <p>
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */
public class _004_LargestPalindromeProduct {

	public static int findLargestPalindromicNumberOf3DigitNumberProduct() {
		int palindrome = 0;
		
		for (int i = 100; i < 1000; i++) {
			for (int j = 100; j < 1000; j++) {
				int num = i*j;
				if (Commons.isPalindrome(num) && (num > palindrome)) {
					palindrome = num;
				}
			}
		}
		
		return palindrome;
	}
	
}