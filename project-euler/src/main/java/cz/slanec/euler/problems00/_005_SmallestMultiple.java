package cz.slanec.euler.problems00;

import java.util.OptionalInt;
import java.util.stream.IntStream;

import cz.slanec.utils.Commons;

/**
 * 2520 is the smallest number that can be divided by each of the numbers
 * from 1 to 10 without any remainder.
 * <p>
 * What is the smallest positive number that is evenly divisible by all
 * of the numbers from 1 to 20?
 */
public class _005_SmallestMultiple {
	
	public static int smallestMultiple() {
		OptionalInt result = IntStream.iterate(20, num -> num + 20)
				.filter(num -> _005_SmallestMultiple.isDivisableByAllTo19(num))
				.findFirst();
		return result.getAsInt();
	}
	
	public static boolean isDivisableByAllTo19(int num) {
		for (int divisor = 19; divisor > 1; divisor--) {
			if (!Commons.isDivisable(num, divisor)) {
				return false;
			}
		}
		return true;
	}
	
}