package cz.slanec.euler.problems;

import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;
import cz.slanec.utils.Commons;

import com.google.common.math.LongMath;

/**
 * A positive fraction whose numerator is less than its denominator is called
 * a proper fraction.
 * <p>
 * For any denominator, d, there will be d−1 proper fractions; for example,
 * with d = 12:<br />
 * 1/12, 2/12, 3/12, 4/12, 5/12, 6/12, 7/12, 8/12, 9/12, 10/12, 11/12.
 * <p>
 * We shall call a fraction that cannot be cancelled down a resilient fraction.<br />
 * Furthermore we shall define the resilience of a denominator, R(d), to be
 * the ratio of its proper fractions that are resilient; for example, R(12) = 4/11.
 * <p>
 * In fact, d = 12 is the smallest denominator having a resilience R(d) < 4/10.
 * <p>
 * Find the smallest denominator d, having a resilience R(d) < 15499/94744 .
 */
public class _243_Resilience {

	public static long smallestResilientDenominatorBelow() {
		// builds Highly composite numbers from low powers of primes
		Set<Long> compositeNumbers = new TreeSet<>();
		Sieve sieve = new Eratosthenes_Array(30);
		for (int prime = 2; prime <= 23; prime = sieve.nextPrime(prime)) {
			Set<Long> tempToAdd = new TreeSet<>();
			for (int exp = 0; exp <= 3; exp++) {
				long pow = Commons.powLong(prime, exp);
				tempToAdd.add(pow);
				for (long storedComposite : compositeNumbers) {
					try {
						tempToAdd.add(LongMath.checkedMultiply(storedComposite, pow));
					} catch (ArithmeticException ignored) {
						// nothing
					}
				}
			}
			compositeNumbers.addAll(tempToAdd);
		}
		
		for (long d : compositeNumbers) {
			if (resilience(d) < 15499d/94744) {
				return d;
			}
		}
		
		throw new NoSuchElementException();
	}
	
	private static double resilience(long n) {
		return Commons.totient(n) /(n-1d);
	}
	
}