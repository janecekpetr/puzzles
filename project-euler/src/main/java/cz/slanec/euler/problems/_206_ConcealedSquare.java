package cz.slanec.euler.problems;

import java.util.List;

import cz.slanec.utils.Commons;
import cz.slanec.utils.Digits;

/**
 * Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,
 * where each "_" is a single digit.
 */
public class _206_ConcealedSquare {
	private static long template = 1020304050607080900L;
	private static final int MAX_INDEX = Commons.length(template) - 1;
	
	public static long getNumberWhoseSquareIs1_2_3_4_5_6_7_8_9_0() {
		long num = (long)Math.sqrt(template);
		
		outerLoop:
		while (true) {
			List<Byte> digits = Digits.decomposeNumber(num*num);

			// the first digit
			byte digit = digits.get(0);
			if (digit > 1) {
				throw new IllegalStateException("One at position 0 not found.");
			}
			
			// for every known digit
			// (the first one and the last zero are forced, don't have to be checked)
			for (int index = 2; index < MAX_INDEX; index += 2) {
				int wantedDigit = index/2 + 1;
				// if the digit is greater than the template
				if (digits.get(index) > wantedDigit) {
					int tempIndex = index-1;
					// while the digit is 9, we can't simply add 1, but must carry it
					while (Digits.digit(template, tempIndex) == 9) {
						template -= 9*Commons.powLong(10, MAX_INDEX-tempIndex);
						tempIndex -= 2;
					}
					// adds 1 to the right digit of the template
					template += Commons.powLong(10, MAX_INDEX-tempIndex);
					// new template ... new lower boundary
					long newNum = (long)Math.sqrt(template);
					// round up to tens
					if (!Commons.isDivisable(newNum, 10)) {
						newNum += 10 - (newNum % 10);
					}
					if (newNum == num) {
						// the change was too weak to change anything, look again
						index = 2;
					} else {
						num = newNum;
						continue outerLoop;
					}
				}
			}
			
			// if some of the digits isn't the right one, continue search
			for (int index = 2; index < MAX_INDEX; index += 2) {
				int wantedDigit = index/2 + 1;
				if (digits.get(index) != wantedDigit) {
					num += 10;
					continue outerLoop;
				}
			}
			
			break;
		}
		
		return num;
	}
	
}