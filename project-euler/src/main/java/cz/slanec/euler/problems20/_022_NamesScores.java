package cz.slanec.euler.problems20;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import cz.slanec.utils.Commons;

import com.google.common.io.Resources;

/**
 * Using names.txt (http://projecteuler.net/project/names.txt), a 46K text file
 * containing over five-thousand first names, begin by sorting it into
 * alphabetical order. Then working out the alphabetical value for each name,
 * multiply this value by its alphabetical position in the list to obtain a name
 * score.
 * <p>
 * For example, when the list is sorted into alphabetical order, COLIN, which
 * is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN
 * would obtain a score of 938 * 53 = 49714.
 * <p>
 * What is the total of all the name scores in the file?
 */
public class _022_NamesScores {

	public static int sumNamesScores() {
		String[] names = null;
		
		try {
			URL input = Resources.getResource("_022_names.txt");
			names = Resources.toString(input, StandardCharsets.UTF_8).replace("\"", "").split(",");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		Arrays.sort(names);
		
		int sum = 0;
		for (int i = 0; i < names.length; i++) {
			sum += Commons.sumWord(names[i]) * (i+1);
		}
		
		return sum;
	}
	
}