package cz.slanec.euler.problems20;

import cz.slanec.factorization.Factorization;
import cz.slanec.utils.Commons;

/**
 * Let d(n) be defined as the sum of proper divisors of n (numbers less
 * than n which divide evenly into n).
 * <p>
 * If d(a) = b and d(b) = a, where a != b, then a and b are an amicable
 * pair and each of a and b are called amicable numbers.
 * <p>
 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20,
 * 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284
 * are 1, 2, 4, 71 and 142; so d(284) = 220.
 * <p>
 * Evaluate the sum of all the amicable numbers under 10000.
 */
public class _021_AmicableNumbers {

	public static int sumAmicableNumbersUnder100() {
		int[] array = new int[10000];
		for (int i = 0; i < array.length; i++) {
			array[i] = Commons.sum(Factorization.divisors(i));
		}
		
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			final int val = array[i];
			if ((val < array.length) && (i == array[val]) && (i != val)) {
				sum += i;
			}
		}
		
		return sum;
	}
	
}