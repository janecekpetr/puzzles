package cz.slanec.euler.problems20;

import cz.slanec.utils.Digits;

import com.google.common.math.BigIntegerMath;

/**
 * n! means n * (n - 1) * ... * 3 * 2 * 1<br />
 * For example, 10! = 10 * 9 * ... * 3 * 2 * 1 = 3628800,
 * and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 * <p>
 * Find the sum of the digits in the number 100!
 */
public class _020_FactorialDigitSum {

	public static int sumDigitsInAFactorial() {
		return Digits.sumDigits(BigIntegerMath.factorial(100).toString());
	}

}