package cz.slanec.euler.problems20;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import cz.slanec.utils.Digits;

import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;

/**
 * A permutation is an ordered arrangement of objects.
 * <p>
 * For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4.
 * If all of the permutations are listed numerically or alphabetically, we call
 * it lexicographic order.<br />
 * The lexicographic permutations of 0, 1 and 2 are: 012, 021, 102, 120, 201, 210
 * <p>
 * What is the millionth lexicographic permutation of the digits
 * 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
 */
public class _024_LexicographicPermutations {

	public static long getMilliontsLexicographicPermutation() {
		Byte[] arr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		
		Collection<List<Byte>> permutations = Collections2.orderedPermutations(Arrays.asList(arr));
		return Digits.composeNumber(Iterables.get(permutations, 999999));
	}

}