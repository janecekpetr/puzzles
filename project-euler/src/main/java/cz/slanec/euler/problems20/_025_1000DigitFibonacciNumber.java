package cz.slanec.euler.problems20;

import java.math.BigInteger;

/**
 * The Fibonacci sequence is defined by the recurrence relation:
 * Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.<br />
 * Hence the first 12 terms will be:<pre>
 *  F1 =   1
 *  F2 =   1
 *  F3 =   2
 *  F4 =   3
 *  F5 =   5
 *  F6 =   8
 *  F7 =  13
 *  F8 =  21
 *  F9 =  34
 * F10 =  55
 * F11 =  89
 * F12 = 144</pre>
 * The 12th term, F12, is the first term to contain three digits.
 * <p>
 * What is the first term in the Fibonacci sequence to contain 1000 digits?
 */
public class _025_1000DigitFibonacciNumber {

	public static int getFirstTermInFibonacci1000DigitsLong() {
		BigInteger first = BigInteger.ZERO;
		BigInteger second = BigInteger.ONE;
		BigInteger sum = first.add(second);
		int term = 2;
		while (sum.toString().length() < 1000) {
			term++;
			first = second;
			second = sum;
			sum = first.add(second);
		}
		return term;
	}

}