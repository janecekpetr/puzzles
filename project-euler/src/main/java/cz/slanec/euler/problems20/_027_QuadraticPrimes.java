package cz.slanec.euler.problems20;

import cz.slanec.factorization.Factorization;

/**
 * Euler published the remarkable quadratic formula: n^2 + n + 41<br />
 * It turns out that the formula will produce 40 primes for the consecutive
 * values n = 0 to 39. However, when n = 40, 402 + 40 + 41 = 40(40 + 1) + 41
 * is divisible by 41, and certainly when n = 41, 41^2 + 41 + 41 is clearly
 * divisible by 41.
 * <p>
 * Using computers, the incredible formula  n^2 − 79n + 1601 was discovered,
 * which produces 80 primes for the consecutive values n = 0 to 79. The product
 * of the coefficients, −79 and 1601, is −126479.
 * <p>
 * Considering quadratics of the form: n^2 + an + b,<br />
 * where |a| < 1000 and |b| < 1000 where |n| is the modulus/absolute
 * value of n, e.g. |11| = 11 and |−4| = 4:
 * <p>
 * Find the product of the coefficients, a and b, for the quadratic expression
 * that produces the maximum number of primes for consecutive values of n,
 * starting with n = 0.
 */
public class _027_QuadraticPrimes {
	private static final int MAX = 1000;
	
	public static int productOfQuadraticCoefficientsThatProduceMostConsecutivePrimes() {
		int maxN = 0;
		int maxA = -1000;
		int maxB = -1000;
		
		for (int a = -MAX; a <= MAX; a++) {
			for (int b = -MAX; b <= MAX; b++) {
				int n = 0;
				while (Factorization.isPrime(n*n + a*n + b)) {
					n++;
				}
				if (n > maxN) {
					maxN = n;
					maxA = a;
					maxB = b;
				}
			}
		}

		return maxA * maxB;
	}
		
}