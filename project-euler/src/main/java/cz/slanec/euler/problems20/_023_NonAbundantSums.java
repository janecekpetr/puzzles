package cz.slanec.euler.problems20;

import java.util.HashSet;
import java.util.Set;

import cz.slanec.factorization.Factorization;
import cz.slanec.utils.Commons;

/**
 * A perfect number is a number for which the sum of its proper divisors is
 * exactly equal to the number. For example, the sum of the proper divisors
 * of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect
 * number.
 * <p>
 * A number n is called deficient if the sum of its proper divisors is less
 * than n and it is called abundant if this sum exceeds n.
 * <p>
 * As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest
 * number that can be written as the sum of two abundant numbers is 24.
 * <p>
 * By mathematical analysis, it can be shown that all integers greater
 * than 28123 can be written as the sum of two abundant numbers. However,
 * this upper limit cannot be reduced any further by analysis even though
 * it is known that the greatest number that cannot be expressed as the sum
 * of two abundant numbers is less than this limit.
 * <p>
 * Find the sum of all the positive integers which cannot be written as
 * the sum of two abundant numbers.
 */
public class _023_NonAbundantSums {

	public static int sumThreeIntegersThatCannotBeSumOfTwoAbundantNumbers() {
		// all abundant numbers
		Set<Integer> allAbundantNumbers = new HashSet<>();
		for (int i = 0; i < 28124; i++) {
			if (Commons.sum(Factorization.divisors(i)) > i) {
				allAbundantNumbers.add(i);
			}
		}
		
		int sum = 0;
		numLoop:
		for (int num = 0; num < 28124; num++) {
			for (int abundant : allAbundantNumbers) {
				if (allAbundantNumbers.contains(num-abundant)) {
					continue numLoop;
				}
			}
			sum += num;
		}
		
		return sum;
	}

}