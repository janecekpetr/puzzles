package cz.slanec.euler.problems20;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * A unit fraction contains 1 in the numerator. The decimal representation
 * of the unit fractions with denominators 2 to 10 are given:<pre>
 *  1/2 = 0.5
 *  1/3 = 0.(3)
 *  1/4 = 0.25
 *  1/5 = 0.2
 *  1/6 = 0.1(6)
 *  1/7 = 0.(142857)
 *  1/8 = 0.125
 *  1/9 = 0.(1)
 * 1/10	= 0.1</pre>
 * Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It
 * can be seen that 1/7 has a 6-digit recurring cycle.
 * <p>
 * Find the value of d < 1000 for which 1/d contains the longest recurring
 * cycle in its decimal fraction part.
 */
public class _026_ReciprocalCycles {
	private static final MathContext precision = new MathContext(3072);
	
	public static int longestRecurringCycleInDecimalFractionPart() {
		int maxD = 0;
		
		int maxLength = 0;
		for (int d = 1000; d > 1; d--) {
			// pruning, e.g. 1/877 can't produce a longer recurring than 876 numbers
			if (maxLength >= d) {
				break;
			}
			BigDecimal b = BigDecimal.ONE.divide(BigDecimal.valueOf(d), precision);
			// get everything after the decimal point
			String str = b.toPlainString();
			str = str.substring(str.indexOf(".") + 1, str.length());
			// if the decimal expansion is short, it's not recurring
			if (str.length() < precision.getPrecision()) {
				continue;
			}
			int curLength = recurringLength(str, d);
			if (curLength > maxLength) {
				maxLength = curLength;
				maxD = d;
			}
		}
		
		return maxD;
	}
	
	private static int recurringLength(String str, int maxLength) {
		// try all lengths from 2 to max possible
		for (int len = 2; len < maxLength; len++) {
			// max offset where the recurring string can begin
			final int maxOffset = str.length() - 3*len;
			for (int offset = 0; (offset < maxOffset); offset++) {
				// test if the pattern occurs consecutively three times
				String sample = str.substring(offset, offset + len);
				if (sample.equals(str.substring(offset + len, offset + 2*len))
						&& sample.equals(str.substring(offset + 2*len, offset + 3*len))) {
					return len;
				}
			}
		}
		return -1;
	}

}