package cz.slanec.euler.problems30;

import cz.slanec.utils.Commons;

/**
 * Surprisingly there are only three numbers that can be written as the sum
 * of fourth powers of their digits:<br />
 * 1634 = 1^4 + 6^4 + 3^4 + 4^4<br />
 * 8208 = 8^4 + 2^4 + 0^4 + 8^4<br />
 * 9474 = 9^4 + 4^4 + 7^4 + 4^4
 * <p>
 * As 1 = 1^4 is not a sum it is not included.
 * <p>
 * The sum of these numbers is 1634 + 8208 + 9474 = 19316.
 * <p>
 * Find the sum of all the numbers that can be written as the sum of fifth
 * powers of their digits.
 */
public class _030_DigitFifthPowers {
	private static final int MAX = 1_000_000;
	private static final int POW = 5;

	public static int sumNumbersThatAreSumsOfFifthPowersOfTheirDigits() {
		int sum = 0;
		
		for (int i = 2; i < MAX; i++) {
			if (i == sumDigitPow(i, POW)) {
				sum += i;
			}
		}
		
		return sum;
	}
	
	private static int sumDigitPow(int num, int pow) {
		int sum = 0;
		while (num > 0) {
			sum += Commons.powInt(num % 10, pow);
			num /= 10;
		}
		return sum;
	}

}