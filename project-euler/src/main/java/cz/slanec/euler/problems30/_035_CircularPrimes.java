package cz.slanec.euler.problems30;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;
import cz.slanec.utils.Commons;

/**
 * The number 197 is called a circular prime because all rotations of
 * the digits: 197, 971, and 719, are themselves prime.
 * <p>
 * There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31,
 * 37, 71, 73, 79, and 97.
 * <p>
 * How many circular primes are there below one million?
 */
public class _035_CircularPrimes {
	private static final Sieve sieve = new Eratosthenes_Array((int)(1.1*1_000_000));
	
	public static int howManyCircularPrimesBelowAMillion() {
		int count = 0;
		for (int prime = 2; prime < 1_000_000; prime = sieve.nextPrime(prime)) {
			if (isCircularPrime(prime)) {
				count++;
			}
		}
		return count;
	}
	
	private static boolean isCircularPrime(int prime) {
		final int max = Commons.length(prime);
		for (int i = 0; i < max; i++) {
			prime = rotate(prime, max);
			if (!sieve.isPrime(prime)) {
				return false;
			}
		}
		return true;
	}
	
	private static int rotate(int num, int length) {
		return (num + Commons.powInt(10, length)*(num % 10)) / 10;
	}
	
}