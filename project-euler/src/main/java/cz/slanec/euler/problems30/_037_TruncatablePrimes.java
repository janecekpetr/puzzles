package cz.slanec.euler.problems30;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;
import cz.slanec.utils.Commons;

/**
 * The number 3797 has an interesting property. Being prime itself, it is
 * possible to continuously remove digits from left to right, and remain
 * prime at each stage: 3797, 797, 97, and 7. Similarly we can work from
 * right to left: 3797, 379, 37, and 3.
 * <p>
 * Find the sum of the only eleven primes that are both truncatable from
 * left to right and right to left.
 * <p>
 * NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
 */
public class _037_TruncatablePrimes {
	
	// no idea why this limit works
	private static final int MAX = 1_000_000;
	private static final Sieve sieve = new Eratosthenes_Array((int)(1.1*MAX));

	public static int sumElevenPrimesTruncatableFromBothSides() {
		int sum = 0;
		for (int prime = sieve.nextPrime(7); prime < MAX; prime = sieve.nextPrime(prime)) {
			if (isTruncatable(prime)) {
				sum += prime;
			}
		}
		return sum;
	}
	
	private static boolean isTruncatable(int prime) {
		int fromLeft = prime;
		int fromRight = prime;
		while (fromRight > 7) {
			fromLeft = fromLeft % (Commons.powInt(10, Commons.length(fromLeft)-1));
			fromRight /= 10;
			if (!sieve.isPrime(fromLeft) || !sieve.isPrime(fromRight)) {
				return false;
			}
		}
		return true;
	}
	
}