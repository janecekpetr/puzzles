package cz.slanec.euler.problems30;

import static cz.slanec.euler.util.PandigitalUtils.validate1to9Num;
import static cz.slanec.euler.util.PandigitalUtils.validateDigits;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import cz.slanec.utils.Commons;

/**
 * We shall say that an n-digit number is pandigital if it makes use of all
 * the digits 1 to n exactly once;<br />
 * for example, the 5-digit number, 15234, is 1 through 5 pandigital.
 * <p>
 * The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing
 * multiplicand, multiplier, and product is 1 through 9 pandigital.
 * <p>
 * Find the sum of all products whose multiplicand/multiplier/product identity
 * can be written as a 1 through 9 pandigital.
 * <p>
 * HINT: Some products can be obtained in more than one way so be sure to only
 * include it once in your sum.
 */
public class _032_PandigitalProducts {
	private static boolean[] digits = new boolean[10];
	
	public static int sumPandigitalNumbers() {
		Set<Integer> products = new HashSet<>();
		
		// multiplicand
		for (int i = 0; i < 100; i++) {
			Arrays.fill(digits, true);
			if (!validate1to9Num(i, digits)) {
				continue;
			}
			
			boolean[] backup = digits;
			// multiplier
			for (int j = i + 1; i + j < 2000; j++) {
				digits = Arrays.copyOf(backup, backup.length);
				if (validate1to9Num(j, digits)
						&& validate1to9Num(i*j, digits)
						&& validateDigits(digits, 1, 9)) {
					products.add(i*j);
				}
			}
		}

		return Commons.sum(products);
	}
	
}