package cz.slanec.euler.problems30;

import cz.slanec.utils.Commons;

/**
 * The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
 * <p>
 * Find the sum of all numbers, less than one million, which are palindromic
 * in base 10 and base 2.
 * <p>
 * (Please note that the palindromic number, in either base, may not include
 * leading zeros.)
 */
public class _036_DoubleBasePalindromes {

	public static int sumDoubleBasePalindromesUnderAMillion() {
		int sum = 0;
		for (int i = 0; i < 1_000_000; i++) {
			if (Commons.isPalindrome(i) && Commons.isPalindrome(Integer.toBinaryString(i))) {
				sum += i;
			}
		}
		return sum;
	}

}