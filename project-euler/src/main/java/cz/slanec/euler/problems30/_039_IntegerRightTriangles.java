package cz.slanec.euler.problems30;

/**
 * If {@code p} is the perimeter of a right angle triangle with integral
 * length sides, {a,b,c}, there are exactly three solutions for p = 120.<br />
 * {20,48,52}, {24,45,51}, {30,40,50}
 * <p>
 * For which value of p <= 1000, is the number of solutions maximised?
 */
public class _039_IntegerRightTriangles {

	public static int perimeterOfTheMostIntegralRightTriangles() {
		int maxSolutions = 0;
		int maxP = 0;
		for (int p = 1; p <= 1000; p++) {
			int solutions = 0;
			// I'm sure tigher limits could be set up, but who cares
			for (int a = 1; a < p; a++) {
				for (int b = a; a+b < p; b++) {
					int c = p - (a+b);
					if (a*a + b*b == c*c) {
						solutions++;
					}
				}
			}
			if (solutions > maxSolutions) {
				maxSolutions = solutions;
				maxP = p;
			}
		}
		return maxP;
	}

}