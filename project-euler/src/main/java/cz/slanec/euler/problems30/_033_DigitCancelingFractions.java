package cz.slanec.euler.problems30;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import cz.slanec.utils.Digits;

import com.google.common.math.IntMath;

/**
 * The fraction 49/98 is a curious fraction, as an inexperienced mathematician
 * in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which
 * is correct, is obtained by cancelling the 9s.
 * <p>
 * We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
 * <p>
 * There are exactly four non-trivial examples of this type of fraction, less
 * than one in value, and containing two digits in the numerator and denominator.
 * <p>
 * If the product of these four fractions is given in its lowest common terms,
 * find the value of the denominator.
 */
public class _033_DigitCancelingFractions {

	public static int denominatorOfProductOfFourDigitCancellingFractions() {
		int[] result = {1, 1};
		
		for (int i = 10; i <= 99; i++) {
			for (int j = i+1; j <= 99; j++) {
				int[] first = {i, j};
				int[] second = {i, j};
				first = simplifyFraction(first);
				second = simplifyCuriously(second);
				if (second == null) {
					// undefined fraction or unsimplifiable fraction
					continue;
				}
				if (Arrays.equals(first, second) && (j % 10 != 0)) {
					result[0] *= i;
					result[1] *= j;
				}
			}
		}
		
		return simplifyFraction(result)[1];
	}
	
	private static int[] simplifyFraction(final int[] fraction) {
		int gcd = IntMath.gcd(fraction[0], fraction[1]);
		int[] ret = {fraction[0] / gcd, fraction[1] / gcd};
		return ret;
	}
	
	private static int[] simplifyCuriously(final int[] fraction) {
		List<Byte> numerator = Digits.decomposeNumber(fraction[0]);
		List<Byte> denominator = Digits.decomposeNumber(fraction[1]);
		
		// remove the same digits
		for (Iterator<Byte> iter = numerator.iterator(); iter.hasNext(); ) {
			if (denominator.remove(iter.next())) {
				iter.remove();
			}
		}
		
		if ((numerator.size() != 1) || (denominator.size() != 1)) {
			return null;
		}
		// returns only the first digits, works only for 2 digit nums
		int[] ret = {numerator.get(0), denominator.get(0)};
		return simplifyFraction(ret);
	}
	
}