package cz.slanec.euler.problems30;

/**
 * 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
 * <p>
 * Find the sum of all numbers which are equal to the sum of the factorial
 * of their digits.
 * <p>
 * Note: as 1! = 1 and 2! = 2 are not sums they are not included.
 */
public class _034_DigitFactorials {
	
	public static int sumNumbersEqualToSumOfTheFactorialOfTheirDigits() {
		int sum = 0;
		// 9! = 362880, 7*362880 ~= 2 540 160, 8 digits don't make sense
		for (int i = 3; i < 10_000_000; i++) {
			if (i == sumOfDigitFactorials(i)) {
				sum += i;
			}
		}
		return sum;
	}
	
	private static int digitFactorial(int num) {
		switch (num) {
			case 0: return      1;
			case 1: return      1;
			case 2: return      2;
			case 3: return      6;
			case 4: return     24;
			case 5: return    120;
			case 6: return    720;
			case 7: return   5040;
			case 8: return  40320;
			case 9: return 362880;
			default: throw new IllegalArgumentException();
		}
	}
	
	private static int sumOfDigitFactorials(int num) {
		int sum = 0;
		while (num > 0) {
			sum += digitFactorial(num % 10);
			num /= 10;
		}
		return sum;
	}
	
}