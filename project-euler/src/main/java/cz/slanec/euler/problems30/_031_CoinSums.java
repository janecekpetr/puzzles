package cz.slanec.euler.problems30;

/**
 * In England the currency is made up of pound, L, and pence, p, and there are
 * eight coins in general circulation:<br />
 * 1p, 2p, 5p, 10p, 20p, 50p, L1 (100p) and L2 (200p).
 * <p>
 * It is possible to make L2 in the following way:<br />
 * 1×L1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
 * <p>
 * How many different ways can L2 be made using any number of coins?
 */
public class _031_CoinSums {
	private static final int[] coins = { 200, 100, 50, 20, 10, 5, 2, 1 };

	public static int howManyWaysToMake2Pounds() {
		return ways(200, 0);
	}

	private static int ways(int amount, int offset) {
		// no money or only pennies left
		if ((amount == 0) || (offset == coins.length - 1)) {
			return 1;
		}
		int sum = 0;
		for (int times = 0; coins[offset] * times <= amount; times++) {
			sum += ways(amount - (coins[offset] * times), offset + 1);
		}
		return sum;
	}

}