package cz.slanec.euler.problems30;

import java.util.Arrays;

import cz.slanec.euler.util.PandigitalUtils;
import cz.slanec.utils.Commons;

/**
 * Take the number 192 and multiply it by each of 1, 2, and 3:<pre>
 * 192 * 1 = 192
 * 192 * 2 = 384
 * 192 * 3 = 576</pre>
 * By concatenating each product we get the 1 to 9 pandigital, 192384576.
 * We will call 192384576 the concatenated product of 192 and (1,2,3).
 * <p>
 * The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4,
 * and 5, giving the pandigital, 918273645, which is the concatenated product
 * of 9 and (1,2,3,4,5).
 * <p>
 * What is the largest 1 to 9 pandigital 9-digit number that can be formed
 * as the concatenated product of an integer with (1,2, ... , n) where n > 1?
 */
public class _038_PandigitalMutiples {
	private static boolean[] digits = new boolean[10];
	
	public static int largestPandigitalFromAnIntegerProduct() {
		int maxProduct = 0;
		
		for (int i = 0; i < 10000; i++) {
			// use all digits again
			Arrays.fill(digits, true);
			
			int result = 0;
			for (int j = 1; j < 10; j++) {
				int product = i*j;
				if (!PandigitalUtils.validate1to9Num(product, digits)) {
					break;
				}
				result = (int)Commons.concatenate(result, product);
				if (Commons.length(result) == 9) {
					maxProduct = Math.max(maxProduct, result);
				}
			}
		}
		return maxProduct;
	}

}