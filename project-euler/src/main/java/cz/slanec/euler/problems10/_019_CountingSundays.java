package cz.slanec.euler.problems10;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * You are given the following information, but you may prefer to do
 * some research for yourself.
 * <ul>
 * 	<li>1 Jan 1900 was a Monday.</li>
 *  <li>Thirty days has September,<br />
 *   April, June and November.<br />
 *   All the rest have thirty-one,<br />
 *   Saving February alone,<br />
 *   Which has twenty-eight, rain or shine.<br />
 *   And on leap years, twenty-nine.<br />
 *  <li>A leap year occurs on any year evenly divisible by 4,
 *  but not on a century unless it is divisible by 400.</li>
 * </ul>
 *  How many Sundays fell on the first of the month during the twentieth century
 *  (1 Jan 1901 to 31 Dec 2000)?
 */
public class _019_CountingSundays {

	public static int countSundaysThatWereFirstInMonthIn20thCentury() {
		final DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		final Calendar cal = Calendar.getInstance();
		int count = 0;
		
		for (int year = 1901; year <= 2000; year++) {
			for (int month = 1; month <= 12; month++) {
				try {
					cal.setTime(format.parse("01." + month + "." + year));
				} catch (ParseException e) {
					// not gonna happen, ever
					throw new RuntimeException(e);
				}
				if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					count++;
				}
			}
		}
		
		return count;
	}

}