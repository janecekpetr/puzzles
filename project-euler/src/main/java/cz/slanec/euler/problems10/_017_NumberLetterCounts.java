package cz.slanec.euler.problems10;

/**
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five,
 * then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 * <p>
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written
 * out in words, how many letters would be used?
 * <p>
 * NOTE: Do not count spaces or hyphens.<br />
 * For example, 342 (three hundred and forty-two) contains 23 letters
 * and 115 (one hundred and fifteen) contains 20 letters. The use of "and"
 * when writing out numbers is in compliance with British usage.
 */
public class _017_NumberLetterCounts {
	private static final int oneToNine = "OneTwoThreeFourFiveSixSevenEightNine".length();
	private static final int tenToNineteen = "TenElevenTwelveThirteenFourteenFifteenSixteenSeventeenEighteenNineteen".length();
	private static final int twentyToNinety = "TwentyThirtyFortyFiftySixtySeventyEightyNinety".length();
	private static final int hundred = "Hundred".length();
	private static final int and = "And".length();
	private static final int thousand = "OneThousand".length();

	public static int howManyLettersInANumber() {
		int count = 0;
		
		count += oneToNine;				// 1-9
		count += tenToNineteen;			// 10-19
		count += twentyToNinety * 10;	// 20-99
		count += oneToNine * 8;
		count *= 10;					// x00-x99
		
		count += hundred * 900;			// 1xx-9xx
		count += and * 99 * 9;
		count += oneToNine * 100;
		
		count += thousand;				// 1000
		
		return count;
	}

}