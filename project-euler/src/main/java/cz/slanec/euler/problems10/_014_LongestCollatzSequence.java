package cz.slanec.euler.problems10;

import cz.slanec.utils.Commons;

/**
 * The following iterative sequence is defined for the set of positive integers:<br />
 * n -> n/2 (n is even)<br />
 * n -> 3n + 1 (n is odd)
 * <p>
 * Using the rule above and starting with 13, we generate the following sequence:
 * 13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
 * <p>
 * It can be seen that this sequence (starting at 13 and finishing at 1)
 * contains 10 terms. Although it has not been proved yet (Collatz Problem), it
 * is thought that all starting numbers finish at 1.
 * <p>
 * Which starting number, under one million, produces the longest chain?<br />
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */
public class _014_LongestCollatzSequence {

	public static int longestCollatzSequenceForNumbersUnderAMillion() {
		int longestNumber = 1;
		int maxChainLength = 1;
		
		for (int start = 2; start < 1000000; start++) {
			int currentLength = 1;
			long temp = start;
			while (temp > 1) {
				temp = collatz(temp);
				currentLength++;
			}
			if (currentLength > maxChainLength) {
				maxChainLength = currentLength;
				longestNumber = start;
			}
		}
		
		return longestNumber;
	}
	
	private static long collatz(long num) {
		return (Commons.isOdd(num)) ? (3*num + 1) : (num/2);
	}

}