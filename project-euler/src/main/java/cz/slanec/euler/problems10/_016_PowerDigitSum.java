package cz.slanec.euler.problems10;

import java.math.BigInteger;

import cz.slanec.utils.Digits;

/**
 * 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 * <p>
 * What is the sum of the digits of the number 2^1000?
 */
public class _016_PowerDigitSum {

	public static int sumDigits() {
		return Digits.sumDigits(BigInteger.valueOf(2).pow(1000).toString());
	}

}