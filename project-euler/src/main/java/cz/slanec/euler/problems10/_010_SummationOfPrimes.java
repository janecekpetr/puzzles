package cz.slanec.euler.problems10;

import cz.slanec.factorization.sieves.Eratosthenes_Array;
import cz.slanec.factorization.sieves.Sieve;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * <p>
 * Find the sum of all the primes below two million.
 */
public class _010_SummationOfPrimes {

	public static long sumPrimesBelow2Million() {
		Sieve sieve = new Eratosthenes_Array((int)(1.1*2_000_000));
		long sum = 0;
		
		int prime = 0;
		while (prime < 2_000_000) {
			sum += prime;
			prime = sieve.nextPrime(prime);
		}
		
		return sum;
	}

}