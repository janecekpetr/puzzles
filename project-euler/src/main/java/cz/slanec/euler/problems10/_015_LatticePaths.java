package cz.slanec.euler.problems10;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Starting in the top left corner of a 2*2 grid, there are 6 routes
 * (without backtracking) to the bottom right corner.
 * <p>
 * How many routes are there through a 20*20 grid?
 */
public class _015_LatticePaths {
	private static final int WIDTH = 20;
	private static final int HEIGHT = 20;
	private static long[][] map = new long[WIDTH+1][HEIGHT+1];

	public static long latticePathsThroughAGrid() {
		// go from bottom right corner to have the smallest number and use them later
		for (int x = WIDTH-1; x >= 0; x--) {
			for (int y = HEIGHT-1; y >= 0; y--) {
				map[x][y] = howManyWays(x, y);
			}
		}
		return map[0][0];
	}
	
	private static long howManyWays(int x, int y) {
		long count = 0;
		Deque<Pair> stack = new ArrayDeque<>();
		stack.add(new Pair(x,y));
		
		while (!stack.isEmpty()) {
			Pair p = stack.removeLast();
			// final cell
			if ((p.x == WIDTH) && (p.y == HEIGHT)) {
				count++;
				continue;
			}
			// dynamic programming, use values computed before
			if (map[p.x][p.y] != 0) {
				count += map[p.x][p.y];
				continue;
			}
			// add a route right
			if (p.x < WIDTH) {
				stack.add(new Pair(p.x+1, p.y));
			}
			// add a route down
			if (p.y < HEIGHT) {
				stack.add(new Pair(p.x, p.y+1));
			}
		}
		
		return count;
	}
	
	private static final class Pair {
		public final int x;
		public final int y;
		
		public Pair(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
}