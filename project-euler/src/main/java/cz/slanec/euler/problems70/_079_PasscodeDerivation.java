package cz.slanec.euler.problems70;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.Iterators;
import com.google.common.io.Resources;

/**
 * A common security method used for online banking is to ask the user for three
 * random characters from a passcode. For example, if the passcode was 531278,
 * they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be:
 * 317.
 * <p>
 * The text file, http://projecteuler.net/project/keylog.txt, contains fifty
 * successful login attempts.
 * <p>
 * Given that the three characters are always asked for in order, analyse the file
 * so as to determine the shortest possible secret passcode of unknown length.
 */
public class _079_PasscodeDerivation {

	public static int shortestPossiblePasscode() {
		List<char[]> orderedPairs = new LinkedList<>();
		
		// read al triplets and build ordered pairs out of them
		try {
			URL input = Resources.getResource("_079_keylog.txt");
			for (String line : Resources.readLines(input, StandardCharsets.UTF_8)) {
				char[] login = line.toCharArray();
				orderedPairs.add(new char[] {login[0], login[1]});
				orderedPairs.add(new char[] {login[1], login[2]});
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// remove a random ordered pair and start on it
		List<Character> order = new LinkedList<>();
		for (char i : orderedPairs.remove((int)(Math.random()*orderedPairs.size()))) {
			order.add(i);
		}

		// merge in all ordered pairs
		Iterator<char[]> iter = Iterators.cycle(orderedPairs);
		while (iter.hasNext()) {
			char[] orderedPair = iter.next();
			if (addInOrder(orderedPair[0], orderedPair[1], order)) {
				iter.remove();
			}
		}
		
		int result = 0;
		for (char c : order) {
			result = result*10 + Character.digit(c, 10);
		}
		return result;
	}

	private static boolean addInOrder(char c, char d, List<Character> order) {
		int indexOfC = order.indexOf(c);
		int indexOfD = order.indexOf(d);
		
		// none there, do nothing
		if ((indexOfC < 0) && (indexOfD < 0)) {
			return false;
		}
		
		// both there, swap or leave
		if ((indexOfC >= 0) && (indexOfD >= 0)) {
			if (indexOfC > indexOfD) {
				Collections.swap(order, indexOfC, indexOfD);
			}
			return true;
		}
		
		// C there, D not
		if (indexOfC >= 0) {
			order.add(d);
		}
		
		// D there, C not
		if (indexOfD >= 0) {
			order.add(0, c);
		}
		
		return true;
	}

}