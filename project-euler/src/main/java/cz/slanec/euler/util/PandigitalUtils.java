package cz.slanec.euler.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Collections2;

public class PandigitalUtils {
	
	private PandigitalUtils() {
		// nothing to do
	}
	
	private static List<Byte> getDigits(int min, int max) {
		List<Byte> list = new ArrayList<>();
		for (int i = min; i <= max; i++) {
			list.add((byte)i);
		}
		return list;
	}
	
	public static Collection<List<Byte>> getPandigitals(int minDigit, int maxDigit) {
		return Collections2.permutations(getDigits(minDigit, maxDigit));
	}
	
	public static boolean validate1to9Num(int num, boolean[] digits) {
		while (num > 0) {
			final int index = num % 10;
			if (((index == 0) || !digits[index])) {
				return false;
			}
			digits[index] = false;
			num /= 10;
		}
		return true;
	}
	
	public static boolean validate0to9Num(long num, boolean[] digits) {
		while (num > 0) {
			final int index = (int)(num % 10);
			if (!digits[index]) {
				return false;
			}
			digits[index] = false;
			num /= 10;
		}
		return true;
	}
	
	public static boolean validateDigits(boolean[] digits, int from, int to) {
		for (int i = from; i <= to; i++) {
			if (digits[i]) {
				return false;
			}
		}
		return true;
	}
}