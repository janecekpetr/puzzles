package cz.slanec.euler.util;

public class PolygonalUtils {
	
	private PolygonalUtils() {
		// nothing to do
	}
	
	public static int p3(int n) {
		return n*(n+1)/2;
	}
	
	public static int p4(int n) {
		return n*n;
	}

	public static int p5(int n) {
		return n*(3*n-1)/2;
	}
	
	public static int p6(int n) {
		return n*(2*n-1);
	}
	
	public static int p7(int n) {
		return n*(5*n-3)/2;
	}
	
	public static int p8(int n) {
		return n*(3*n-2);
	}
}