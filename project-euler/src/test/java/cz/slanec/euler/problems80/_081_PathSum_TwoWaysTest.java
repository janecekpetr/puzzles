package cz.slanec.euler.problems80;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _081_PathSum_TwoWaysTest {
	
	@Test
	public void testPathSum_TwoWays() {
		assertEquals(427337, _081_PathSum_TwoWays.minimalPathSum());
	}

}