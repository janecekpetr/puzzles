package cz.slanec.euler.problems00;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _004_LargestPalindromeProductTest {

	@Test
	public void testFindLargestPalindromicNumberOf3DigitNumberProduct() {
		assertEquals(906609, _004_LargestPalindromeProduct.findLargestPalindromicNumberOf3DigitNumberProduct());
	}
	
}