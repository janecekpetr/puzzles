package cz.slanec.euler.problems00;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _009_SpecialPythagoreanTripletTest {
	
	@Test
	public void testSpecialPythagoreanTriplet() {
		assertEquals(31875000, _009_SpecialPythagoreanTriplet.getSpecialPythagoreanTriplet());
	}

}