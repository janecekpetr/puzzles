package cz.slanec.euler.problems00;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _001_MultiplesOf3And5Test {

	@Test
	public void testSum3sAnd5s() {
		assertEquals(233168, _001_MultiplesOf3And5.sum3sAnd5s(1000));
	}

}