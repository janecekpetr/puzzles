package cz.slanec.euler.problems00;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _003_LargestPrimeFactorTest {
	
	@Test
	public void testLargestPrimeFactor() {
		assertEquals(6857L, _003_LargestPrimeFactor.getLastPrimeFactor(600851475143L));
	}

}