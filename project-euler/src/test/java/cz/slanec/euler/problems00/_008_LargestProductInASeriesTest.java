package cz.slanec.euler.problems00;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _008_LargestProductInASeriesTest {
	
	@Test
	public void testLagestProductInASeries() {
		assertEquals(40824, _008_LargestProductInASeries.getLargestProductInASeries());
	}

}