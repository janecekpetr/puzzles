package cz.slanec.euler.problems00;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _002_EvenFibonacciNumbersTest {
	
	@Test
	public void testSumEvenFibonacciNumbers() {
		assertEquals(4613732, _002_EvenFibonacciNumbers.sumEvenFibonacciNumbers(4_000_000));
	}

}