package cz.slanec.euler.problems00;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _005_SmallestMultipleTest {
	
	@Test
	public void testSmallestMultiple() {
		assertEquals(232792560, _005_SmallestMultiple.smallestMultiple());
	}
	
}