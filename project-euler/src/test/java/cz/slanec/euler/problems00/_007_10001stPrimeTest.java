package cz.slanec.euler.problems00;

import static org.junit.Assert.*;

import org.junit.Test;

public class _007_10001stPrimeTest {
	
	@Test
	public void test10001stPrime() {
		assertEquals(104743, _007_10001stPrime.get10001stPrime());
	}

}