package cz.slanec.euler.problems00;

import static org.junit.Assert.*;

import org.junit.Test;

public class _006_SumSquareDifferenceTest {
	
	@Test
	public void testSumSquareDifference() {
		assertEquals(25164150, _006_SumSquareDifference.sumSquareDifference());
	}

}