package cz.slanec.euler.problems60;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _061_CyclicalFigurateNumbersTest {
	
	@Test
	public void testCyclicalFigurateNumbers() {
		assertEquals(28684, _061_CyclicalFigurateNumbers.sumCyclicalFigurateNumbers());
	}

}