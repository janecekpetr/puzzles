package cz.slanec.euler.problems60;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _069_TotientMaximumTest {
	
	@Test
	public void testTotientMaximum() {
		assertEquals(510510, _069_TotientMaximum.maxRatioOfNAndTotientNBelowAMillion());
	}

}