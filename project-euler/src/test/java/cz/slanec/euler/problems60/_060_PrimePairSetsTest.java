package cz.slanec.euler.problems60;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _060_PrimePairSetsTest {
	
	@Test
	public void testPrimePairSets() {
		assertEquals(26033, _060_PrimePairSets.lowestSumOfFivePrimesThatConcatenatedAlsoProduceAPrime());
	}

}