package cz.slanec.euler.problems60;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _063_PowerfulDigitCountsTest {
	
	@Test
	public void testPowerfulDigitCounts() {
		assertEquals(49, _063_PowerfulDigitCounts.howManyNDigitIntegersAreAlsoNthPower());
	}

}