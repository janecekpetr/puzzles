package cz.slanec.euler.problems60;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _067_MaximumPathSumIITest {
	
	@Test
	public void testMaximumPathSumII() {
		assertEquals(7273, _067_MaximumPathSumII.findMaximumSumPathInATriangle());
	}

}