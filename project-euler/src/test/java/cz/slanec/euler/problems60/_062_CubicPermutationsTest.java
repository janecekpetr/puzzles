package cz.slanec.euler.problems60;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _062_CubicPermutationsTest {
	
	@Test
	public void testCubicPermutations() {
		assertEquals(127035954683L, _062_CubicPermutations.smallestCubeWhichHasFiveCubePermutations());
	}

}