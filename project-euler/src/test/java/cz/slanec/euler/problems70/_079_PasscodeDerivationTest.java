package cz.slanec.euler.problems70;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _079_PasscodeDerivationTest {
	
	@Test
	public void testPasscodeDerivation() {
		assertEquals(73162890, _079_PasscodeDerivation.shortestPossiblePasscode());
	}

}