package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _031_CoinSumsTest {
	
	@Test
	public void testCoinSums() {
		assertEquals(73682, _031_CoinSums.howManyWaysToMake2Pounds());
	}

}