package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _036_DoubleBasePalindromesTest {
	
	@Test
	public void testDoubleBasePalindromes() {
		assertEquals(872187, _036_DoubleBasePalindromes.sumDoubleBasePalindromesUnderAMillion());
	}

}