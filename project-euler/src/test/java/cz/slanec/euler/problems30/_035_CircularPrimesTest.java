package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _035_CircularPrimesTest {
	
	@Test
	public void testCircularPrimes() {
		assertEquals(55, _035_CircularPrimes.howManyCircularPrimesBelowAMillion());
	}

}