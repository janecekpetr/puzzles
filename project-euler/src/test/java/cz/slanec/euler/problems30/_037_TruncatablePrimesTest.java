package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _037_TruncatablePrimesTest {
	
	@Test
	public void testTruncatablePrimes() {
		assertEquals(748317, _037_TruncatablePrimes.sumElevenPrimesTruncatableFromBothSides());
	}

}