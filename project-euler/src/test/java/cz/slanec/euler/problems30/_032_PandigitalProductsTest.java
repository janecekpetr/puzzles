package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _032_PandigitalProductsTest {
	
	@Test
	public void testPandigitalProducts() {
		assertEquals(45228, _032_PandigitalProducts.sumPandigitalNumbers());
	}

}