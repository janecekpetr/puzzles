package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _039_IntegerRightTrianglesTest {
	
	@Test
	public void testIntegerRightTriangles() {
		assertEquals(840, _039_IntegerRightTriangles.perimeterOfTheMostIntegralRightTriangles());
	}

}