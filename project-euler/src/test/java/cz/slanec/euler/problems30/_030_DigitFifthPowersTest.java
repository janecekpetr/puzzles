package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _030_DigitFifthPowersTest {
	
	@Test
	public void testDigitFifthPowers() {
		assertEquals(443839, _030_DigitFifthPowers.sumNumbersThatAreSumsOfFifthPowersOfTheirDigits());
	}

}