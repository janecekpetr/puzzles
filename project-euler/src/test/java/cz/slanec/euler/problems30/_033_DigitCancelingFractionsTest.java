package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _033_DigitCancelingFractionsTest {
	
	@Test
	public void testDigitCancelingFractions() {
		assertEquals(100, _033_DigitCancelingFractions.denominatorOfProductOfFourDigitCancellingFractions());
	}

}