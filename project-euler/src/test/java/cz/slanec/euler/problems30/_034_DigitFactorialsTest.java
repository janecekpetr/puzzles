package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _034_DigitFactorialsTest {
	
	@Test
	public void testDigitFactorials() {
		assertEquals(40730, _034_DigitFactorials.sumNumbersEqualToSumOfTheFactorialOfTheirDigits());
	}

}