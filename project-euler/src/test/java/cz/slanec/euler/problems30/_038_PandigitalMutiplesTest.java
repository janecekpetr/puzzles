package cz.slanec.euler.problems30;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _038_PandigitalMutiplesTest {
	
	@Test
	public void testPandigitalMultiples() {
		assertEquals(932718654, _038_PandigitalMutiples.largestPandigitalFromAnIntegerProduct());
	}

}