package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _056_PowerfulDigitSumTest {
	
	@Test
	public void testPowerfulDigitSum() {
		assertEquals(972, _056_PowerfulDigitSum.maximumPowerfulSum());
	}

}