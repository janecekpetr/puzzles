package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _057_SquareRootConvergentsTest {
	
	@Test
	public void testSquareRootConvergents() {
		assertEquals(153, _057_SquareRootConvergents.howManyFractionsContainANumeratorWithMoreDigitsThanDenominator());
	}

}