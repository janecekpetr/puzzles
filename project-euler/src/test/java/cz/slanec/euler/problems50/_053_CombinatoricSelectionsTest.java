package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _053_CombinatoricSelectionsTest {
	
	@Test
	public void testCombinatoricSelections() {
		assertEquals(4075, _053_CombinatoricSelections.howManyCombinationsAreGreaterThanAMillion());
	}

}