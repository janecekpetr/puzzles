package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _050_ConsecutivePrimeSumTest {
	
	@Test
	public void testConsecutivePrimeSum() {
		assertEquals(997651, _050_ConsecutivePrimeSum.primeWhichIsWrittenAsASumOfTheMostConsecutivePrimes());
	}

}