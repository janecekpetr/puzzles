package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _059_XORDecryptionTest {
	
	@Test
	public void testXORDecryption() {
		assertEquals(107359, _059_XORDecryption.sumThreeCharXorKey());
	}

}