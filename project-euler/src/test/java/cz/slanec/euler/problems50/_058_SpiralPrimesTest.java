package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _058_SpiralPrimesTest {
	
	@Test
	public void testSpiralPrimes() {
		assertEquals(26241, _058_SpiralPrimes.howLongWillSpiralSideBeWhenTheRatioOnDiagonalsFallsBelow10Percent());
	}

}