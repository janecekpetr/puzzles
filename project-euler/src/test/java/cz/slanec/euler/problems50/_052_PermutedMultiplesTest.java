package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _052_PermutedMultiplesTest {
	
	@Test
	public void testPermutedMultiples() {
		assertEquals(142857, _052_PermutedMultiples.smallestIntegerWhichMultipliedUsesSameDigits());
	}

}