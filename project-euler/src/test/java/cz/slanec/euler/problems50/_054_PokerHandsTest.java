package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _054_PokerHandsTest {
	
	@Test
	public void testPokerHands() {
		assertEquals(376, _054_PokerHands.howManyHandsWin());
	}

}