package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _055_LychrelNumbersTest {
	
	@Test
	public void testLychrelNumbers() {
		assertEquals(249, _055_LychrelNumbers.howManyLychrelNumbersBelow10000());
	}

}