package cz.slanec.euler.problems50;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _051_PrimeDigitReplacementsTest {
	
	@Test
	public void testPrimeDigitReplacements() {
		assertEquals(121313, _051_PrimeDigitReplacements.smalestPrimeWhichIsPartOfAnEightPrimeFamilyWhenSubnumberReplaced());
	}

}