package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _018_MaximumPathSumITest {
	
	@Test
	public void testMaximumPathSumI() {
		assertEquals(1074, _018_MaximumPathSumI.findMaximumSumPathInATriangle());
	}

}