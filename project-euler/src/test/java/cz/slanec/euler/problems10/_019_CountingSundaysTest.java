package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _019_CountingSundaysTest {
	
	@Test
	public void testCountingSundays() {
		assertEquals(171, _019_CountingSundays.countSundaysThatWereFirstInMonthIn20thCentury());
	}

}