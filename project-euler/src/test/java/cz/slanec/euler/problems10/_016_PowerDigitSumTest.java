package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _016_PowerDigitSumTest {
	
	@Test
	public void testPowerDigitSum() {
		assertEquals(1366, _016_PowerDigitSum.sumDigits());
	}

}