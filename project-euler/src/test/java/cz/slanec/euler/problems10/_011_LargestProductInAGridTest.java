package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _011_LargestProductInAGridTest {
	
	@Test
	public void testLargestProductInAGrid() {
		assertEquals(70600674, _011_LargestProductInAGrid.getLargestProductInAGrid());
	}

}