package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _012_HighlyDivisibleTriangularNumberTest {
	
	@Test
	public void testHighlyDivisibleTriangularNumberTest() {
		assertEquals(76576500, _012_HighlyDivisibleTriangularNumber.getTriangleNumberWithOver500Divisors());
	}

}