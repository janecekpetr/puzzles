package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _010_SummationOfPrimesTest {
	
	@Test
	public void testSUmmationOfPrimes() {
		assertEquals(142913828922L, _010_SummationOfPrimes.sumPrimesBelow2Million());
	}

}