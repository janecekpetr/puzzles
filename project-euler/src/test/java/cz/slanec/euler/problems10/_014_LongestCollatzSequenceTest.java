package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _014_LongestCollatzSequenceTest {
	
	@Test
	public void testLogestCollatzSequence() {
		assertEquals(837799, _014_LongestCollatzSequence.longestCollatzSequenceForNumbersUnderAMillion());
	}

}