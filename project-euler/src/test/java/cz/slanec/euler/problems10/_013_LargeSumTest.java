package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _013_LargeSumTest {
	
	@Test
	public void testLargeSum() {
		assertEquals(5537376230L, _013_LargeSum.sumLargeNumbers());
	}

}