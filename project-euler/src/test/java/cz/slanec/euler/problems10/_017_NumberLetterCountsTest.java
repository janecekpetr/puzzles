package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _017_NumberLetterCountsTest {
	
	@Test
	public void testNumberLetterCounts() {
		assertEquals(21124, _017_NumberLetterCounts.howManyLettersInANumber());
	}

}