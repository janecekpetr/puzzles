package cz.slanec.euler.problems10;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _015_LatticePathsTest {
	
	@Test
	public void testLatticePaths() {
		assertEquals(137846528820L, _015_LatticePaths.latticePathsThroughAGrid());
	}

}