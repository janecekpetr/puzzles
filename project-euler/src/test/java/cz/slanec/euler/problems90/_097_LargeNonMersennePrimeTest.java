package cz.slanec.euler.problems90;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _097_LargeNonMersennePrimeTest {
	
	@Test
	public void testLargeNonMersennePrimeByHand() {
		assertEquals(8739992577L, _097_LargeNonMersennePrime.lastTenDigitsOfALargeMersennePrimeByHand());
	}
	
	@Test
	public void testLargeNonMersennePrimeBigIntegerWay() {
		assertEquals(8739992577L, _097_LargeNonMersennePrime.lastTenDigitsOfALargeMersennePrimeBigIntegerWay());
	}

}