package cz.slanec.euler.problems90;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _095_AmicableChainsTest {
	
	@Test
	public void testAmicableChains() {
		assertEquals(14316, _095_AmicableChains.smallestMemberOfLongestAmicableChainWithNoElementOverAMillion());
	}

}