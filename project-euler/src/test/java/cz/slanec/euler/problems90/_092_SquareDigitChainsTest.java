package cz.slanec.euler.problems90;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _092_SquareDigitChainsTest {
	
	@Test
	public void testSquareDigitChains() {
		assertEquals(8581146, _092_SquareDigitChains.howManyNumbersBelow10MillionArriveAt89ByChainingSumsOfDigitSquares());
	}

}