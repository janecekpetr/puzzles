package cz.slanec.euler.problems;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _243_ResilienceTest {
	
	@Test
	public void testResilience() {
		assertEquals(892371480, _243_Resilience.smallestResilientDenominatorBelow());
	}

}