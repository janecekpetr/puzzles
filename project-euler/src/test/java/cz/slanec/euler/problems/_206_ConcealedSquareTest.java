package cz.slanec.euler.problems;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _206_ConcealedSquareTest {
	
	@Test
	public void testConcealedSquare() {
		assertEquals(1389019170, _206_ConcealedSquare.getNumberWhoseSquareIs1_2_3_4_5_6_7_8_9_0());
	}

}