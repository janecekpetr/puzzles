package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _026_ReciprocalCyclesTest {
	
	@Test
	public void testReciprocalCycles() {
		assertEquals(983, _026_ReciprocalCycles.longestRecurringCycleInDecimalFractionPart());
	}

}