package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _021_AmicableNumbersTest {
	
	@Test
	public void testAmicableNumbers() {
		assertEquals(31626, _021_AmicableNumbers.sumAmicableNumbersUnder100());
	}

}