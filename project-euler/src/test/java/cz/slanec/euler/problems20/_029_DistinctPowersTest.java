package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _029_DistinctPowersTest {
	
	@Test
	public void testDistinctPowers() {
		assertEquals(9183, _029_DistinctPowers.howManyDistinctTermsGeneratedByExponent());
	}

}