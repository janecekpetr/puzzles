package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _020_FactorialDigitSumTest {
	
	@Test
	public void testFactorialDigitSum() {
		assertEquals(648, _020_FactorialDigitSum.sumDigitsInAFactorial());
	}

}