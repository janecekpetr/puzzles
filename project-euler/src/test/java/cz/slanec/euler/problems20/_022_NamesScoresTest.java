package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _022_NamesScoresTest {
	
	@Test
	public void testNamesScores() {
		assertEquals(871198282, _022_NamesScores.sumNamesScores());
	}

}