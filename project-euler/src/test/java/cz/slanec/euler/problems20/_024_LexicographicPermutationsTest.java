package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _024_LexicographicPermutationsTest {
	
	@Test
	public void testLexicographicPermutations() {
		assertEquals(2783915460L, _024_LexicographicPermutations.getMilliontsLexicographicPermutation());
	}

}