package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _025_1000DigitFibonacciNumberTest {
	
	@Test
	public void test1000DigitFibonacciNumber() {
		assertEquals(4782, _025_1000DigitFibonacciNumber.getFirstTermInFibonacci1000DigitsLong());
	}

}