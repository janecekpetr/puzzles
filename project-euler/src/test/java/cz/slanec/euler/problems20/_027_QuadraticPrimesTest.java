package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _027_QuadraticPrimesTest {
	
	@Test
	public void testQuadraticPrimes() {
		assertEquals(-59231, _027_QuadraticPrimes.productOfQuadraticCoefficientsThatProduceMostConsecutivePrimes());
	}

}