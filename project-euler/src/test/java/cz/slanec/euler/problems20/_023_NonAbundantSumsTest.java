package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _023_NonAbundantSumsTest {
	
	@Test
	public void testNonAbundantSums() {
		assertEquals(4179871, _023_NonAbundantSums.sumThreeIntegersThatCannotBeSumOfTwoAbundantNumbers());
	}

}