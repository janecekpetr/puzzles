package cz.slanec.euler.problems20;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _028_NumberSpiralDiagonalsTest {
	
	@Test
	public void testNumberSpiralDiagonals() {
		assertEquals(669171001, _028_NumberSpiralDiagonals.sumOfDiagonalsOfA1001Times1001Spiral());
	}

}