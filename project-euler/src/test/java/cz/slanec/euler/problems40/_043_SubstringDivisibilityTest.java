package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _043_SubstringDivisibilityTest {
	
	@Test
	public void testSubstringDivisibility() {
		assertEquals(16695334890L, _043_SubstringDivisibility.sumPandigitalNumbersWithNicelyDivisibleSubstrings());
	}

}