package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _047_DistinctPrimeFactorsTest {
	
	@Test
	public void testDistinctPrimeFactors() {
		assertEquals(134043, _047_DistinctPrimeFactors.firstFourIntegersToHaveFourDistincsPrimeFactors());
	}

}