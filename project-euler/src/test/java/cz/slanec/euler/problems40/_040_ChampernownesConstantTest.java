package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _040_ChampernownesConstantTest {
	
	@Test
	public void testChampernownesConstant() {
		assertEquals(210, _040_ChampernownesConstant.champernownesConstant());
	}

}