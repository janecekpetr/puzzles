package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _044_PentagonNumbersTest {
	
	@Test
	public void testPentagonNumbers() {
		assertEquals(5482660, _044_PentagonNumbers.differenceOfPentagonalsWhoseSumAndDiffArePentagonal());
	}

}