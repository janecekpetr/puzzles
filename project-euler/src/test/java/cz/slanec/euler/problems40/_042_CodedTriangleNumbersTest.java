package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _042_CodedTriangleNumbersTest {
	
	@Test
	public void testCodedTriangleNumbers() {
		assertEquals(162, _042_CodedTriangleNumbers.howManyTriangleWords());
	}

}