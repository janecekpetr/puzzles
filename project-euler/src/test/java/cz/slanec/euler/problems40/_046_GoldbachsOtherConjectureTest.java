package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _046_GoldbachsOtherConjectureTest {
	
	@Test
	public void testGoldbachsOtherConjecture() {
		assertEquals(5777, _046_GoldbachsOtherConjecture.smallestCompositeThatIsNotASumOfPrimeAndTwiceASquare());
	}

}