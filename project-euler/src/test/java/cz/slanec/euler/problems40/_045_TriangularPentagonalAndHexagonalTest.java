package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _045_TriangularPentagonalAndHexagonalTest {
	
	@Test
	public void testTriangularPentagonalAndHexagonal() {
		assertEquals(1533776805, _045_TriangularPentagonalAndHexagonal.trianglePentagonalHexagonalNumber());
	}

}