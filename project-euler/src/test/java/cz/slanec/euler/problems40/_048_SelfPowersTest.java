package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _048_SelfPowersTest {
	
	@Test
	public void testSelfPowers() {
		assertEquals(9110846700L, _048_SelfPowers.lastTenDigitsOfSumOfSelfPowersUpToAThousand());
	}

}