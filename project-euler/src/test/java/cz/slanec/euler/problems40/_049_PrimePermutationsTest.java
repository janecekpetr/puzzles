package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _049_PrimePermutationsTest {
	
	@Test
	public void testPrimePermutations() {
		assertEquals(296962999629L, _049_PrimePermutations.concatenateNumbersThatAreAllPrimesAndPermutations());
	}

}