package cz.slanec.euler.problems40;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class _041_PandigitalPrimeTest {
	
	@Test
	public void testPandigitalPrime() {
		assertEquals(7652413, _041_PandigitalPrime.largestPandigitalPrime());
	}

}