package cz.slanec.codewars.kyu7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TriangularTest {
	@Test
	public void testTwoShouldBeThree() throws Exception {
		assertEquals(3, Triangular.triangular(2));
	}

	@Test
	public void testFourShouldBeTen() throws Exception {
		assertEquals(10, Triangular.triangular(4));
	}
}