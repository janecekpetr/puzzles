package cz.slanec.codewars.kyu8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MultiplyTest {
	
	@Test
	public void testMultiplication() {
		assertEquals(Double.valueOf(6d), Multiply.multiply(2d, 3d));
	}

}