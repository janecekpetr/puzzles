package cz.slanec.codewars.kyu8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PersonTest {
	
	@Test
	public void testGreeting() {
		assertEquals("Hi Slanec, my name is Petr", new Person("Petr").greet("Slanec"));
	}

}