package cz.slanec.codewars.kyu6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StairsTest {
	@Test
	public void test1() {
		assertEquals(6, Stairs.NumberOfSteps(10, 2));
	}

	@Test
	public void test2() {
		assertEquals(-1, Stairs.NumberOfSteps(3, 5));
	}

	@Test
	public void test3() {
		assertEquals(10, Stairs.NumberOfSteps(10, 10));
	}

	@Test
	public void test4() {
		assertEquals(3, Stairs.NumberOfSteps(6, 3));
	}
}