package cz.slanec.codewars.kyu6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GroupsTest {

	@Test
	public void myTests() {
		assertEquals(true, Groups.groupCheck("()"));
		assertEquals(false, Groups.groupCheck("({"));
		assertEquals(false, Groups.groupCheck("({})))]]]"));
	}

}
