package cz.slanec.codewars.kyu6;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class WhichAreInTest {
	@Test
	public void test1() {
		String[] a = { "arp", "live", "strong" };
		String[] b = { "lively", "alive", "harp", "sharp", "armstrong" };
		String[] r = { "arp", "live", "strong" };
		assertArrayEquals(r, WhichAreIn.inArray(a, b));
	}
}