package cz.slanec.codewars.kyu6;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PigLatinTest {

	private PigLatin p;

	@Before
	public void setUp() throws Exception {
		p = new PigLatin();

	}

	@Test
	public void testMap() {
		assertEquals("apmay", p.translate("map"));
	}

	@Test
	public void testEgg() {
		assertEquals("eggway", p.translate("egg"));
	}

	@Test
	public void testSpaghetti() {
		assertEquals("aghettispay", p.translate("spaghetti"));
	}

	@Test
	public void testCCCC() {
		assertEquals("ccccay", p.translate("CCCC"));
	}

	@Test
	public void test123() {
		assertEquals(null, p.translate("123"));
	}

	@Test
	public void testTes3t5() {
		assertEquals(null, p.translate("Tes3t5"));
	}

}