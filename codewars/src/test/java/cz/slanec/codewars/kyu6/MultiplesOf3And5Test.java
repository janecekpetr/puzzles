package cz.slanec.codewars.kyu6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MultiplesOf3And5Test {
	@Test
	public void test() {
		assertEquals(23, new MultiplesOf3And5().solution(10));
	}
}