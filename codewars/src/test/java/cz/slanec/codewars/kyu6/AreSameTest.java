package cz.slanec.codewars.kyu6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AreSameTest {
	@Test
	public void test1() {
		int[] a = { 121, 144, 19, 161, 19, 144, 19, 11 };
		int[] b = { 121, 14641, 20736, 361, 25921, 361, 20736, 361 };
		assertEquals(true, AreSame.comp(a, b));
	}

	@Test
	public void test2() {
		int[] a = { 121, 144, 19, 161, 19, 144, 19, 11 };
		int[] b = { 132, 14641, 20736, 361, 25921, 361, 20736, 361 };
		assertEquals(false, AreSame.comp(a, b));
	}

	@Test
	public void test3() {
		int[] a = { 121, 144, 19, 161, 19, 144, 19, 11 };
		int[] b = { 121, 14641, 20736, 36100, 25921, 361, 20736, 361 };
		assertEquals(false, AreSame.comp(a, b));
	}
}