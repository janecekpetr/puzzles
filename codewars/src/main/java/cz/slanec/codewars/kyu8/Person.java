package cz.slanec.codewars.kyu8;

public class Person {
	private final String name;

	public Person(String personName) {
		name = personName;
	}

	public String greet(String yourName) {
		return String.format("Hi %s, my name is %s", yourName, name);
	}
	
}