package cz.slanec.codewars.kyu7;

public class VowelToIndex {
	
	  public static String vowel2Index(String str) {
		  StringBuilder result = new StringBuilder();
		  for (int index = 0; index < str.length(); index++) {
			  char c = str.charAt(index);
			  if (isVowel(c)) {
				  result.append(index + 1);
			  } else {
				  result.append(c);
			  }
		  }
	      return result.toString();
	  }
	  
	  private static boolean isVowel(char c) {
		  c = Character.toLowerCase(c);
		  return (c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u');
	  }

}