package cz.slanec.codewars.kyu7;

public class CompoundArray {

	public static int[] compoundArray(int[] a, int[] b) {
		final int shorter = Math.min(a.length, b.length);
		
		int[] result = new int[a.length + b.length];
		for (int i = 0; i < shorter; i++) {
			result[2*i] = a[i];
			result[2*i + 1] = b[i];
		}
		
		int[] longerArray = (a.length > b.length) ? a : b;
		for (int i = shorter; i < longerArray.length; i++) {
			result[shorter + i] = longerArray[i];
		}
		
		return result;
	}
	
}