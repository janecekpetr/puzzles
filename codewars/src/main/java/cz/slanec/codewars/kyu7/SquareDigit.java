package cz.slanec.codewars.kyu7;

public class SquareDigit {

	public int squareDigits(int n) {
		return String.valueOf(n)
				.chars()
				.map(digit -> (digit - '0'))
				.map(digit -> (digit * digit))
				.reduce(0, (result, number) -> ((result * powInt(10, length(number))) + number));
	}

	private static int powInt(int base, int exp) {
		if (exp == 0) {
			return 1;
		}

		// for low exponents, obvious multiplication is faster by a lot
		if (exp <= 10) {
			int result = base;
			while (exp-- > 1) {
				result *= base;
			}
			return result;
		}

		// exponentiation by squaring
		int result = 1;
		while (exp > 0) {
			if ((exp & 1) == 1) {
				result *= base;
			}
			base *= base;
			exp >>= 1;
		}
		return result;
	}

	private static int length(long number) {
		// the minus sign or the special zero case have one more digit
		int length = (number <= 0) ? 1 : 0;
		while (number != 0) {
			length++;
			number /= 10;
		}
		return length;
	}

}