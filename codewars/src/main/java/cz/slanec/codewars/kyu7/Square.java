package cz.slanec.codewars.kyu7;

public class Square {

	public static boolean isSquare(int n) {
		return isInteger(Math.sqrt(n));
	}

	private static boolean isInteger(double d) {
		return (d == (int)d);
	}

}