package cz.slanec.codewars.kyu7;

public class RowSumOddNumbers {

	public static int rowSumOddNumbers(int n) {
		int base = (n - 1) * n + 1;
		int result = 0;
		for (int i = 0; i < n; i++) {
			result += base + (i * 2);
		}
		return result;
	}
	
}