package cz.slanec.codewars.kyu7;

public class Triangular {

	public static int triangular(int n) {
		if (n < 0) {
			return 0;
		}
		int result = n*(n+1)/2;
		return Math.max(0, result);
	}

}