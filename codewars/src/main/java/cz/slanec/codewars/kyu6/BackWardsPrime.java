package cz.slanec.codewars.kyu6;

import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class BackWardsPrime {

	public static String backwardsPrime(long start, long end) {
		return LongStream.rangeClosed(start, end)
			.filter(value -> isPrime(value))
			.filter(num -> {
				long reverse = reverse(num);
				return (num != reverse) && isPrime(reverse);
			})
			.mapToObj(Long::toString)
			.collect(Collectors.joining(" "));
	}

	public static boolean isPrime(long number) {
		if (number < 2) {
			return false;
		}
		if (number == 2 || number == 3 || number == 5) {
			return true;
		}
		if ((number % 2 == 0) || (number % 3 == 0) || (number % 5 == 0)) {
			return false;
		}
		long max = (long)Math.sqrt(number);
		for (long i = 0; i <= max; i += 30) {
			if (		number % (i +  7) == 0
					|| (number % (i + 11) == 0)
					|| (number % (i + 13) == 0)
					|| (number % (i + 17) == 0)
					|| (number % (i + 19) == 0)
					|| (number % (i + 23) == 0)
					|| (number % (i + 29) == 0)
					|| (number % (i + 31) == 0) ) {
				return (number <= i+31);
			}
		}
		return true;
	}

	private static long reverse(long num) {
		long result = 0;
		while (num > 0) {
			result = (result * 10) + (num % 10);
			num /= 10;
		}
		return result;
	}

}