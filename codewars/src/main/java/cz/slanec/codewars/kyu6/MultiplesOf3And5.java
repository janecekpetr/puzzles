package cz.slanec.codewars.kyu6;

import java.util.stream.IntStream;

public class MultiplesOf3And5 {

	public int solution(int number) {
		return IntStream.range(3, number)
				.filter(num -> (num % 3 == 0) || (num % 5 == 0))
				.sum();
	}

}