package cz.slanec.codewars.kyu6;

public class Stairs {

	public static int NumberOfSteps(int steps, int targetDivisor) {
		int minimumSteps = (steps + 1) / 2;
		if (steps < targetDivisor) {
			return -1;
		}
		if (minimumSteps % targetDivisor == 0) {
			return minimumSteps;
		}
		return minimumSteps + targetDivisor - (minimumSteps % targetDivisor);
	}

}