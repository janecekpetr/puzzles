package cz.slanec.codewars.kyu6;

import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.IntStream;

public class AreSame {

	public static boolean comp(int[] a, int[] b) {
		if ((a == null) || (b == null)) {
			return false;
		}

		MultiSet<Integer> squares = new MultiSet<>();
		for (int num : a) {
			squares.add(num * num);
		}

		return IntStream.of(b)
			.allMatch(num -> squares.remove(num));
	}
	
	@SuppressWarnings("unused")
	private static class MultiSet<E> extends AbstractSet<E> {
		private final Map<E, Integer> counters = new HashMap<>();

		@Override
		public boolean add(E e) {
			int count = counters.getOrDefault(e, 0);
			counters.put(e, count + 1);
			return true;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public boolean remove(Object o) {
			int count = counters.getOrDefault(o, 0) - 1;
			if (count <= 0) {
				return (counters.remove(o) != null);
			}
			return (counters.put((E)o, count) != null);
		}
		
		public int count(E e) {
			return counters.getOrDefault(e, 0);
		}
		
		@Override
		public boolean contains(Object o) {
			return counters.containsKey(o);
		}

		@Override
		public Iterator<E> iterator() {
			return counters.keySet().iterator();
		}

		@Override
		public int size() {
			return counters.size();
		}
	}

}