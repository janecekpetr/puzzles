package cz.slanec.codewars.kyu6;

import java.util.ArrayDeque;
import java.util.Deque;

public class Groups {

	public static boolean groupCheck(String str) {
		Deque<Character> stack = new ArrayDeque<>();

		for (char c : str.toCharArray()) {
			if (isOpening(c)) {
				stack.add(c);
			} else if (stack.isEmpty() || !closes(stack.removeLast(), c)) {
				return false;
			}
		}

		if (stack.isEmpty()) {
			return true;
		}
		return false;
	}

	private static boolean isOpening(char c) {
		return (c == '{') || (c == '[') || (c == '(');
	}

	private static boolean closes(char opening, char closing) {
		return ((opening == '{') && (closing == '}'))
				|| ((opening == '[') && (closing == ']'))
				|| ((opening == '(') && (closing == ')'));
	}

}