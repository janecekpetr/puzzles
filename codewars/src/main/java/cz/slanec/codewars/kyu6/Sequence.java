package cz.slanec.codewars.kyu6;

public class Sequence {

	public static long getScore(long n) {
		return n*(n+1)*25;
	}
	
}