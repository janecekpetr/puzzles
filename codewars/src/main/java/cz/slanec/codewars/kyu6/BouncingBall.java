package cz.slanec.codewars.kyu6;

public class BouncingBall {

	public static int bouncingBall(double h, double bounce, double window) {
		if (h <= 0 || bounce <= 0 || bounce >= 1 || window >= h) {
			return -1;
		}

		return 2 * (int)log(bounce, window / h) + 1;
	}

	private static double log(double base, double num) {
		return Math.log(num) / Math.log(base);
	}

}