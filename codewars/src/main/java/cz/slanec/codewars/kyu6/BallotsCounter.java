package cz.slanec.codewars.kyu6;

import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BallotsCounter {

	public static String getWinner(List<String> listOfBallots) {
		MultiSet<String> candidateCounts = new MultiSet<>();
		for (String vote : listOfBallots) {
			candidateCounts.add(vote);
		}
		
		for (String candidate : candidateCounts) {
			if (candidateCounts.count(candidate) > listOfBallots.size() / 2) {
				return candidate;
			}
		}
		
		return null;
	}

	private static class MultiSet<E> extends AbstractSet<E> {
		private final Map<E, Integer> counters = new HashMap<>();

		@Override
		public boolean add(E e) {
			int count = counters.getOrDefault(e, 0);
			counters.put(e, count + 1);
			return true;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public boolean remove(Object o) {
			int count = counters.getOrDefault(o, 0) - 1;
			if (count <= 0) {
				return (counters.remove(o) != null);
			}
			return (counters.put((E)o, count) != null);
		}
		
		public int count(E e) {
			return counters.getOrDefault(e, 0);
		}
		
		@Override
		public boolean contains(Object o) {
			return counters.containsKey(o);
		}

		@Override
		public Iterator<E> iterator() {
			return counters.keySet().iterator();
		}

		@Override
		public int size() {
			return counters.size();
		}
	}

}