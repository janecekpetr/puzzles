package cz.slanec.codewars.kyu6;

import java.util.Set;
import java.util.TreeSet;

public class WhichAreIn {

	public static String[] inArray(String[] array1, String[] array2) {
		Set<String> results = new TreeSet<>();
		outer:
		for (String needle : array1) {
			for (String haystack : array2) {
				if (haystack.contains(needle)) {
					results.add(needle);
					continue outer;
				}
			}
		}
		return results.toArray(new String[results.size()]);
	}

}