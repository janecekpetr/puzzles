package cz.slanec.codewars.kyu6;

public class PigLatin {

	public String translate(String str) {
		if (str.chars().anyMatch(i -> !Character.isLetter(i))) {
			return null;
		}
		
		str = str.toLowerCase();

		int index = 0;
		while ((index < str.length()) && !isVowel(str.charAt(index))) {
			index++;
		}

		if (index == 0) {
			return str + "way";
		}

		return str.substring(index) + str.substring(0, index) + "ay";
	}

	private static boolean isVowel(char c) {
		c = Character.toLowerCase(c);
		return (c == 'a') || (c == 'e') || (c == 'i') || (c == 'o') || (c == 'u');
	}

}