package cz.slanec.codewars.kyu5;

import java.util.HashMap;
import java.util.Map;

public class PerfectPower {
	
	private static final int MAX = (int)Math.sqrt(Integer.MAX_VALUE);
	private static final int MAX_EXPONENT = 31;
	
	private static final Map<Integer, int[]> PERFECT_POWERS = new HashMap<>();
	static {
		for (int i = 2; i < MAX; i++) {
			for (int exp = 2; exp < MAX_EXPONENT; exp++) {
				long perfectPower = powLong(i, exp);
				if (perfectPower < Integer.MAX_VALUE) {
					PERFECT_POWERS.put((int)perfectPower, new int[] { i, exp });
				} else {
					break;
				}
			}
		}
	}
	
	public static int[] isPerfectPower(int n) {
		return PERFECT_POWERS.get(n);
	}
	
	public static long powLong(long base, int exp) {
		if (exp == 0) {
			return 1;
		}
		
		// for low exponents, obvious multiplication is faster by a lot
		long result = base;
		while (exp --> 1) {
			result *= base;
		}
		return result;
	}
	
}